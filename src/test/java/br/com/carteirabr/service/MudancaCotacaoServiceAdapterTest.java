package br.com.carteirabr.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.util.CarteiraQuarkusTest;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class MudancaCotacaoServiceAdapterTest extends CarteiraQuarkusTest {
	
	@Inject
	AcoesService acoesService;
	
	@Inject
	AcaoConsolidadaRepositoryPort acaoConsolidadaRepository;
	
	@Test
	public void giveSymbol_ChangeCotacaoNextMonth_ShouldConsolidar() {
		var acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol("MGLU3F");
		acaoConsolidada.setCpf("30167389882");
		acaoConsolidada.setData(LocalDate.of(2020, Month.JULY, 30));
		acaoConsolidada.setQuantidade(new BigDecimal(1));
		acaoConsolidada.setValorTotal(new BigDecimal(50));
		acaoConsolidada.setPosicaoAtual(BigDecimal.ZERO);
		acaoConsolidadaRepository.persist(acaoConsolidada);
		
		var cotacao = new Cotacao();
		cotacao.setSymbol("MGLU3F");
		cotacao.setPreco(new BigDecimal(1.50));
		
		acoesService.mudancaCotacaoHandler(cotacao);
		
		assertEquals(2, acaoConsolidadaRepository.count("symbol", "MGLU3F"));
	}
	
	@Test
	public void giveSymbol_ChangeCotacaoSameMonth_ShouldConsolidarNotGenerareAnotherRow() {
		var acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol("BBDC4F");
		acaoConsolidada.setCpf("30167389882");
		acaoConsolidada.setData(LocalDate.of(2020, Month.AUGUST, 30));
		acaoConsolidada.setQuantidade(new BigDecimal(1));
		acaoConsolidada.setValorTotal(new BigDecimal(50));
		acaoConsolidada.setPosicaoAtual(BigDecimal.ZERO);
		acaoConsolidadaRepository.persist(acaoConsolidada);
		
		var cotacao = new Cotacao();
		cotacao.setSymbol("MGLU3F");
		cotacao.setPreco(new BigDecimal(1.50));
		
		acoesService.mudancaCotacaoHandler(cotacao);
		
		assertEquals(1, acaoConsolidadaRepository.count("symbol", "BBDC4F"));
	}
	
}
