package br.com.carteirabr.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.service.FiiService;

@ExtendWith(MockitoExtension.class)
class FiiServiceTest {

	@InjectMocks
	FiiService fiiService = new FiiService();
	
	@BeforeEach
	public void before() {
		ServiceContext.install(new ServiceContext());
		ServiceContext.get().overrideCurrentUsername("301.673.898-82");
	}
	
	@Test
	void givenProventos_shouldConsolidate_returnCorrectvalue() {
		var proventos = new ArrayList<Provento>();
		
		var proventoA = new Provento();
		proventoA.setTotal(new BigDecimal(0.65));
		proventoA.setData(LocalDate.of(2020, Month.APRIL, 15));
		proventos.add(proventoA);
		
		var proventoB = new Provento();
		proventoB.setTotal(new BigDecimal(0.65));
		proventoB.setData(LocalDate.of(2020, Month.APRIL, 15));
		proventos.add(proventoB);
		
		var proventoC = new Provento();
		proventoC.setTotal(new BigDecimal(0.63));
		proventoC.setData(LocalDate.of(2020, Month.MARCH, 15));
		proventos.add(proventoC);
		
		var consolidacao = fiiService.consolidarProventos(proventos.stream());
		
		assertFalse(proventos.isEmpty());
		assertNotNull(consolidacao);
		assertEquals(2, consolidacao.size());
		assertEquals(1.3, consolidacao.get(0).getTotal().doubleValue());
		assertEquals(0.63, consolidacao.get(1).getTotal().doubleValue());
	}
	
	@Test
	void givenProventosDiferentDate_shouldConsolidate_returnCorrectvalue() {
		var proventos = new ArrayList<Provento>();
		
		var proventoA = new Provento();
		proventoA.setTotal(new BigDecimal(0.65));
		proventoA.setData(LocalDate.of(2020, Month.APRIL, 10));
		proventos.add(proventoA);
		
		var proventoB = new Provento();
		proventoB.setTotal(new BigDecimal(0.65));
		proventoB.setData(LocalDate.of(2020, Month.APRIL, 5));
		proventos.add(proventoB);
		
		var proventoC = new Provento();
		proventoC.setTotal(new BigDecimal(0.63));
		proventoC.setData(LocalDate.of(2020, Month.MARCH, 15));
		proventos.add(proventoC);
		
		var consolidacao = fiiService.consolidarProventos(proventos.stream());
		
		assertFalse(proventos.isEmpty());
		assertNotNull(consolidacao);
		assertEquals(2, consolidacao.size());
		assertEquals(1.3, consolidacao.get(0).getTotal().doubleValue());
		assertEquals(0.63, consolidacao.get(1).getTotal().doubleValue());
	}
	
	@Test
	void givenProventosDiferentValues_shouldConsolidate_returnCorrectvalue() {
		var proventos = new ArrayList<Provento>();
		
		var proventoA = new Provento();
		proventoA.setTotal(new BigDecimal(1.65));
		proventoA.setData(LocalDate.of(2020, Month.APRIL, 10));
		proventos.add(proventoA);
		
		var proventoB = new Provento();
		proventoB.setTotal(new BigDecimal(1.65));
		proventoB.setData(LocalDate.of(2020, Month.APRIL, 5));
		proventos.add(proventoB);
		
		var proventoC = new Provento();
		proventoC.setTotal(new BigDecimal(0.63));
		proventoC.setData(LocalDate.of(2020, Month.MARCH, 15));
		proventos.add(proventoC);
		
		var consolidacao = fiiService.consolidarProventos(proventos.stream());
		
		assertFalse(proventos.isEmpty());
		assertNotNull(consolidacao);
		assertEquals(2, consolidacao.size());
		assertEquals(3.3, consolidacao.get(0).getTotal().doubleValue());
		assertEquals(0.63, consolidacao.get(1).getTotal().doubleValue());
	}
	
	@Test
	void givenProventosZeroValues_shouldConsolidate_returnCorrectvalue() {
		var proventos = new ArrayList<Provento>();
		
		var proventoA = new Provento();
		proventoA.setTotal(BigDecimal.ZERO);
		proventoA.setData(LocalDate.of(2020, Month.APRIL, 10));
		proventos.add(proventoA);
		
		var proventoB = new Provento();
		proventoB.setTotal(new BigDecimal(1.65));
		proventoB.setData(LocalDate.of(2020, Month.APRIL, 5));
		proventos.add(proventoB);
		
		var proventoC = new Provento();
		proventoC.setTotal(new BigDecimal(0.63));
		proventoC.setData(LocalDate.of(2020, Month.MARCH, 15));
		proventos.add(proventoC);
		
		var consolidacao = fiiService.consolidarProventos(proventos.stream());
		
		assertFalse(proventos.isEmpty());
		assertNotNull(consolidacao);
		assertEquals(2, consolidacao.size());
		assertEquals(1.65, consolidacao.get(0).getTotal().doubleValue());
		assertEquals(0.63, consolidacao.get(1).getTotal().doubleValue());
	}

}
