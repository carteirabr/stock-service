package br.com.carteirabr.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.WebApplicationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.port.UserRepositoryPort;

@ExtendWith(MockitoExtension.class)
public class AddUserServiceAdapterTest {
	
	@Mock
	private UserRepositoryPort userRepository;
	
	@InjectMocks
	private AddUserServiceAdapter addUserServiceAdapter;
	
	@Test
	public void givenAValidUser_itCallsTheUserRepository() {
		String nameOfUser = "alexandre";
		String password = "test";
		
		Usuario newUser = new Usuario();
		newUser.setUsername(nameOfUser);
		newUser.setPassword(password);
		newUser.setEmail("alexandreaw@gmail.com");
		newUser.setNome("Alexandre");
		newUser.setSobreNome("Aleixo");
		newUser.setRole("USER");
		
		when(userRepository.count(anyString(), anyString())).thenReturn(0L);

		Usuario userPersist = addUserServiceAdapter.addUser(newUser);

		verify(userRepository).persist(newUser);
		
		assertEquals(nameOfUser, userPersist.getUsername());
	}
	
	@Test
	public void givenAValidUser_itShouldEncryptPassword() {
		String nameOfUser = "alexandre1";
		String password = "test";
		
		Usuario newUser = new Usuario();
		newUser.setUsername(nameOfUser);
		newUser.setPassword(password);

		Usuario userPersist = addUserServiceAdapter.addUser(newUser);

		assertTrue(userPersist.validatePassword(password));
	}
	
	@Test
	public void givenAComplexPassword_itShouldEncryptPassword() {
		String nameOfUser = "alexandre2";
		String password = "test#010";
		
		Usuario newUser = new Usuario();
		newUser.setUsername(nameOfUser);
		newUser.setPassword(password);

		Usuario userPersist = addUserServiceAdapter.addUser(newUser);

		assertTrue(userPersist.validatePassword(password));
	}
	
	@Test
	public void givenRepitedUser_itShouldError() {
		String nameOfUser = "alexandre2";
		String password = "test#010";
		
		Usuario newUser = new Usuario();
		newUser.setUsername(nameOfUser);
		newUser.setPassword(password);
		
		when(userRepository.count(anyString(), anyString())).thenReturn(1L);

		WebApplicationException thrown = assertThrows(
				WebApplicationException.class,
		           () -> addUserServiceAdapter.addUser(newUser),
		           "Esperado erro ao adicionar usuario repetido"
		    );

		assertEquals(thrown.getMessage(), "Usuario com cpf alexandre2 já cadastrado.");
	}
}
