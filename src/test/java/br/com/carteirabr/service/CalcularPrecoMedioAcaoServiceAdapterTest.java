package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.util.SymbolUtil;

@ExtendWith(MockitoExtension.class)
public class CalcularPrecoMedioAcaoServiceAdapterTest {
	
	@InjectMocks
	private CalcularPrecoMedioAcaoServiceAdapter calcularPrecoMedioAcaoServiceAdapter;

	@Test
	public void givenOrdem_itReturnMedialPrice() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		BigDecimal quantidade = new BigDecimal("2");
		BigDecimal valorTotal = new BigDecimal("69.220");

		/* Comprar papel */
		
		BigDecimal precoMedio = calcularPrecoMedioAcaoServiceAdapter.calcularPrecoMedio(valorTotal, quantidade, acaoConsolidada);
		
		assertThat(precoMedio, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(34.61)))));
	}
	
	@Test
	public void givenOrdem_withZeroAmount_ShouldReturnZero() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		BigDecimal quantidade = new BigDecimal("0");
		BigDecimal valorTotal = new BigDecimal("69.220");

		/* Comprar papel */
		
		BigDecimal precoMedio = calcularPrecoMedioAcaoServiceAdapter.calcularPrecoMedio(valorTotal, quantidade, acaoConsolidada);
		
		assertThat(precoMedio, is(equalTo(BigDecimal.ZERO)));
	}
	
	@Test
	public void givenOrdem_withZeroRoundedAmount_ShouldReturnZero() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setPrecoMedio(BigDecimal.ZERO);
		acaoConsolidada.setSymbol(symbol);
		
		BigDecimal quantidade = SymbolUtil.converterDecimal(new BigDecimal("0"));
		BigDecimal valorTotal = new BigDecimal("69.220");

		/* Comprar papel */
		
		BigDecimal precoMedio = calcularPrecoMedioAcaoServiceAdapter.calcularPrecoMedio(valorTotal, quantidade, acaoConsolidada);
		
		assertThat(precoMedio, is(equalTo(BigDecimal.ZERO)));
	}

}
