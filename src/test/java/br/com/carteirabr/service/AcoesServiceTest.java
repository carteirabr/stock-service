package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.service.AcoesService;
import br.com.carteirabr.util.CarteiraQuarkusTest;
import br.com.carteirabr.util.SymbolUtil;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;


@QuarkusTest
public class AcoesServiceTest extends CarteiraQuarkusTest {
	
	@Inject
	private AcoesService acoesService;
	
	@Inject
	private OrdemRepositoryPort ordemRepository;
	
	@Inject
	AcaoConsolidadaRepositoryPort acaoConsolidadaRepository;
	
	@Test
	public void testGerarHistoricoConsolidadoSymbol() {
		String symbol = "FBOK34";
		DateTimeFormatter formater = DateTimeFormatter.ofPattern("MM/yyyy");

		/* Cenario */
		LocalDate MES_6 = LocalDate.now();
		Ordem stockOrder = new Ordem();
		stockOrder.setCpf(ServiceContext.get().getCurrentUsername());
		stockOrder.symbol = symbol;
		stockOrder.data = MES_6;
		stockOrder.orderType = "C";
		stockOrder.amount = new BigDecimal("5");
		stockOrder.factor = new BigDecimal("2");
		stockOrder.value = new BigDecimal("110.22");
		stockOrder.totalValue = new BigDecimal("1102.20");
		ordemRepository.persist(stockOrder);
		
		LocalDate MES_5 = LocalDate.now().minusMonths(1);
		Ordem stockOrder2 = new Ordem();
		stockOrder2.setCpf(ServiceContext.get().getCurrentUsername());
		stockOrder2.symbol = symbol;
		stockOrder2.data = MES_5;
		stockOrder2.orderType = "C";
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("138.36");
		stockOrder2.totalValue = new BigDecimal("138.36");
		ordemRepository.persist(stockOrder2);
		
		LocalDate MES_5_2 = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).minusMonths(1);
		Ordem stockOrder3 = new Ordem();
		stockOrder3.setCpf(ServiceContext.get().getCurrentUsername());
		stockOrder3.symbol = symbol;
		stockOrder3.data = MES_5_2;
		stockOrder3.orderType = "C";
		stockOrder3.amount = new BigDecimal("1");
		stockOrder3.factor = new BigDecimal("1");
		stockOrder3.value = new BigDecimal("138.36");
		stockOrder3.totalValue = new BigDecimal("138.36");
		ordemRepository.persist(stockOrder3);
		
		acoesService.consolidaHistorico(symbol);
		
		/* Validação */
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(new BigDecimal(12));
		BigDecimal expectedQuantidadeMes5 = SymbolUtil.converterDecimal(new BigDecimal(2));
		
		AcaoConsolidada acaoConsolidada = acaoConsolidadaRepository.find("{ symbol: ?1, mesAno: ?2 }", symbol, formater.format(MES_6)).firstResult();
		AcaoConsolidada acaoConsolidada_mes5 = acaoConsolidadaRepository.find("{ symbol: ?1, mesAno: ?2 }", symbol, formater.format(MES_5)).firstResult();
		
		acaoConsolidadaRepository.stream("{ symbol: ?1 } ", symbol)
			.map(AcaoConsolidada.class::cast)
			.forEach(a -> {
			System.out.println(a.getCpf() + "-" + a.getData() );
		});
		
		assertThat(acaoConsolidadaRepository.count("symbol", symbol), is(equalTo(2L)));
		assertThat(acaoConsolidada.getQuantidade(), is(equalTo(expectedQuantidade)));
		assertThat(acaoConsolidada_mes5.getQuantidade(), is(equalTo(expectedQuantidadeMes5)));
	}
	
	@Test
	public void testSymbolChangeTest() {
		String symbol = "MGLU3";

		Ordem stockOrder = new Ordem();
		stockOrder.symbol = symbol;
		stockOrder.orderType = "C";
		stockOrder.data = LocalDate.now();
		stockOrder.amount = new BigDecimal("2");
		stockOrder.factor = new BigDecimal("10");
		stockOrder.value = new BigDecimal("138.36");
		stockOrder.totalValue = new BigDecimal("2767.2");
		
		/* Primeira mudança */
		AcaoConsolidada acaoConsolidada = acoesService.consolidateSymbol(stockOrder);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("128.10");
		stockOrder2.totalValue = new BigDecimal("120.10");
		
		/* Segunda Mudança */
		acaoConsolidada = acoesService.consolidateSymbol(stockOrder2);
		
		/* Validação */
		BigDecimal expectedValorTotal = SymbolUtil.converterDecimal(new BigDecimal(2887.30));
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(new BigDecimal(21));
		BigDecimal expectedPrecoMedio = SymbolUtil.converterDecimal(new BigDecimal(137.49));
		
		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbol)));
		assertThat(acaoConsolidadaRepository.count("symbol", symbol), is(equalTo(1L)));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
		assertThat(acaoConsolidada.getQuantidade(), is(equalTo(expectedQuantidade)));
		assertThat(acaoConsolidada.getPosicaoAtual(), is(notNullValue()));
		assertThat(acaoConsolidada.getPrecoMedio(), is(equalTo(expectedPrecoMedio)));
	}
	
	@Test
	public void testSymbolFracionado() {
		/* TEST */
		
		String symbolF = "XPLG11";

		Ordem stockOrder = new Ordem();
		stockOrder.symbol = symbolF;
		stockOrder.orderType = "C";
		stockOrder.data = LocalDate.now();
		stockOrder.amount = new BigDecimal("1");
		stockOrder.factor = new BigDecimal("1");
		stockOrder.value = new BigDecimal("138.36");
		stockOrder.totalValue = new BigDecimal("138.36");
		
		/* PRIMEIRA MUDANÇA */
		AcaoConsolidada acaoConsolidada = acoesService.consolidateSymbol(stockOrder);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbolF;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("128.10");
		stockOrder2.totalValue = new BigDecimal("128.10");
		
		/* SEGUNDA MUDANÇA */
		acaoConsolidada = acoesService.consolidateSymbol(stockOrder2);
		
		/* VALIDAÇÃO */
		BigDecimal expectedValorTotal = new BigDecimal(266.46);
		expectedValorTotal = expectedValorTotal.setScale(2, RoundingMode.HALF_UP);
		
		BigDecimal expectedQuantidade = new BigDecimal(2);
		expectedQuantidade = expectedQuantidade.setScale(2, RoundingMode.HALF_UP);
		
		BigDecimal expectedPrecoMedio = new BigDecimal(133.23);
		expectedPrecoMedio = expectedPrecoMedio.setScale(2, RoundingMode.HALF_UP);
		
		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbolF)));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
		assertThat(acaoConsolidada.getQuantidade(), is(equalTo(expectedQuantidade)));
		assertThat(acaoConsolidada.getPosicaoAtual(), is(notNullValue()));
		assertThat(acaoConsolidada.getPrecoMedio(), is(equalTo(expectedPrecoMedio)));
		
		PanacheMock.reset();
	}

	@Test
	public void testCompraVendaZerandoPosicaoAcoes() {
		String symbolF = "MGLU3F";

		Ordem stockOrder = new Ordem();
		stockOrder.symbol = symbolF;
		stockOrder.orderType = "C";
		stockOrder.data = LocalDate.now();
		stockOrder.amount = new BigDecimal("1");
		stockOrder.factor = new BigDecimal("1");
		stockOrder.value = new BigDecimal("67.36");
		stockOrder.totalValue = new BigDecimal("67.36");

		/* Primeira mudança */
		AcaoConsolidada acaoConsolidada = acoesService.consolidateSymbol(stockOrder);

		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbolF;
		stockOrder2.orderType = "V";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");

		acaoConsolidada = acoesService.consolidateSymbol(stockOrder2);

		/* Validação */		
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		BigDecimal expectedPosicaoAtual = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		BigDecimal expectedValorTotal = SymbolUtil.converterDecimal(new BigDecimal(69.20));
		BigDecimal expectedLucroPrejuizo = SymbolUtil.converterDecimal(new BigDecimal(1.84));

		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbolF)));
		assertThat(acaoConsolidada.getQuantidade(), is((equalTo(expectedQuantidade))));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
		assertThat(acaoConsolidada.getPosicaoAtual(), is((equalTo(expectedPosicaoAtual))));
		assertThat(acaoConsolidada.getLucroPrejuizo(), is((equalTo(expectedLucroPrejuizo))));
	}
	
	@Test
	public void testCompraVendaAcoes() {
		String symbolF = "VIVA3F";

		Ordem stockOrder = new Ordem();
		stockOrder.symbol = symbolF;
		stockOrder.orderType = "C";
		stockOrder.data = LocalDate.now();
		stockOrder.amount = new BigDecimal("2");
		stockOrder.factor = new BigDecimal("1");
		stockOrder.value = new BigDecimal("67.36");
		stockOrder.totalValue = new BigDecimal("134.72");

		/* Primeira mudança */
		AcaoConsolidada acaoConsolidada = acoesService.consolidateSymbol(stockOrder);

		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbolF;
		stockOrder2.orderType = "V";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");

		acaoConsolidada = acoesService.consolidateSymbol(stockOrder2);

		/* Validação */		
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(new BigDecimal(1));
//		BigDecimal expectedPosicaoAtual = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		BigDecimal expectedValorTotal = SymbolUtil.converterDecimal(new BigDecimal(65.52));
//		BigDecimal expectedLucroPrejuizo = SymbolUtil.converterDecimal(new BigDecimal(1.84));

		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbolF)));
		assertThat(acaoConsolidada.getQuantidade(), is((equalTo(expectedQuantidade))));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
//		assertThat(acaoConsolidada.getPosicaoAtual(), is((equalTo(expectedPosicaoAtual))));
//		assertThat(acaoConsolidada.getLucroPrejuizo(), is((equalTo(expectedLucroPrejuizo))));
	}
	

}
