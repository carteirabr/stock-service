package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.util.SymbolUtil;

@ExtendWith(MockitoExtension.class)
public class CalcularLucroPrejuizoAcaoServiceAdapterTest {
	
	@InjectMocks
	private CalcularLucroPrejuizoAcaoServiceAdapter calcularPrecoAtualServiceAdapter;

	@Test
	public void givenValues_iPositiveQtde_tReturnLucroPrejuizoPositive() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setValorTotal(new BigDecimal("10.33"));
		
		BigDecimal quantidade = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		BigDecimal valorToal = new BigDecimal("15.00");
		BigDecimal posicaoAtual = new BigDecimal("11.00");

		/* Comprar papel */
		
		BigDecimal lucroPrejuizo = calcularPrecoAtualServiceAdapter.calcularLucroPrejuizo(quantidade, valorToal, posicaoAtual, acaoConsolidada);
		
		assertThat(lucroPrejuizo, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(4.67)))));
	}
	
	@Test
	public void givenValues_itReturnLucroPrejuizo() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setValorTotal(new BigDecimal("10.33"));
		
		BigDecimal quantidade = new BigDecimal("1");
		BigDecimal valorToal = new BigDecimal("15.00");
		BigDecimal posicaoAtual = new BigDecimal("11.00");

		/* Comprar papel */
		
		BigDecimal lucroPrejuizo = calcularPrecoAtualServiceAdapter.calcularLucroPrejuizo(quantidade, valorToal, posicaoAtual, acaoConsolidada);
		
		assertThat(lucroPrejuizo, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(-4.00)))));
	}
	
	@Test
	public void givenValues_withZeroQtde_itReturnLucroPrejuizo() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setValorTotal(new BigDecimal("20.33"));
		
		BigDecimal quantidade = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		BigDecimal valorToal = new BigDecimal("15.00");
		BigDecimal posicaoAtual = new BigDecimal("11.00");

		/* Comprar papel */
		
		BigDecimal lucroPrejuizo = calcularPrecoAtualServiceAdapter.calcularLucroPrejuizo(quantidade, valorToal, posicaoAtual, acaoConsolidada);
		
		assertThat(lucroPrejuizo, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(-5.33)))));
	}
}
