package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.util.CarteiraQuarkusTest;
import br.com.carteirabr.util.SymbolUtil;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ConsolidarSymbolServiceAdapterTest extends CarteiraQuarkusTest {
	
	@Inject
	ConsolidarSymbolService consolidarSymbolServiceAdapter;
	
	@Inject
	OrdemRepositoryPort ordemRepository;
	
	@Inject
	AcaoConsolidadaRepositoryPort acaoRepository;
	
	@Test
	public void givenOrdens_BuyAndSell_ShouldeReturnCorrectConsolidation() {
		String symbol = "HGLG11";
		String cpf = ServiceContext.get().getCurrentUsername();

		/* Cenario */
		LocalDate MES_5 = LocalDate.of(2020, Month.AUGUST, 5);
		
		Ordem stockOrder = new Ordem();
		stockOrder.setCpf(cpf);
		stockOrder.symbol = symbol;
		stockOrder.orderType = "C";
		stockOrder.data = MES_5;
		stockOrder.amount = new BigDecimal("5");
		stockOrder.factor = new BigDecimal("2");
		stockOrder.value = new BigDecimal("110.22");
		stockOrder.totalValue = new BigDecimal("1102.20");
		ordemRepository.persist(stockOrder);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.setCpf(cpf);
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "V";
		stockOrder2.data = MES_5;
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("138.36");
		stockOrder2.totalValue = new BigDecimal("138.36");
		ordemRepository.persist(stockOrder2);
		
		/* Execução */
		AcaoConsolidada acaoConsolidada = consolidarSymbolServiceAdapter.reConsolidateSymbol(symbol);
		
		/* Validação */
		BigDecimal expectedValorTotal = SymbolUtil.converterDecimal(new BigDecimal(963.84));
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(new BigDecimal(9));
		BigDecimal expectedPrecoMedio = SymbolUtil.converterDecimal(new BigDecimal(107.09));
		
		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbol)));
		assertThat(acaoRepository.count("symbol", symbol), is(equalTo(1L)));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
		
		assertThat(acaoConsolidada.getQuantidade(), is(equalTo(expectedQuantidade)));
		assertThat(acaoConsolidada.getPosicaoAtual(), is(notNullValue()));
		assertThat(acaoConsolidada.getPrecoMedio(), is(equalTo(expectedPrecoMedio)));
	}
	
	@Test
	public void givenOrdens_BuyAndSellOnDate_ShouldeReturnCorrectConsolidation() {
		String symbol = "ITSA4";
		String cpf = ServiceContext.get().getCurrentUsername();

		/* Cenario */
		LocalDate MES_5 = LocalDate.of(2020, Month.AUGUST, 5);
		
		Ordem stockOrder = new Ordem();
		stockOrder.setCpf(cpf);
		stockOrder.symbol = symbol;
		stockOrder.orderType = "C";
		stockOrder.data = MES_5;
		stockOrder.amount = new BigDecimal("5");
		stockOrder.factor = new BigDecimal("2");
		stockOrder.value = new BigDecimal("110.22");
		stockOrder.totalValue = new BigDecimal("1102.20");
		ordemRepository.persist(stockOrder);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.setCpf(cpf);
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "V";
		stockOrder2.data = MES_5;
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("138.36");
		stockOrder2.totalValue = new BigDecimal("138.36");
		ordemRepository.persist(stockOrder2);
		
		/* Execução */
		AcaoConsolidada acaoConsolidada = consolidarSymbolServiceAdapter.reConsolidateSymbol(symbol, MES_5);
		
		/* Validação */
		BigDecimal expectedValorTotal = SymbolUtil.converterDecimal(new BigDecimal(963.84));
		BigDecimal expectedQuantidade = SymbolUtil.converterDecimal(new BigDecimal(9));
		BigDecimal expectedPrecoMedio = SymbolUtil.converterDecimal(new BigDecimal(107.09));
		
		assertThat(acaoConsolidada, is(notNullValue()));
		assertThat(acaoConsolidada.getSymbol(), is(equalTo(symbol)));
		assertThat(acaoRepository.count("symbol", symbol), is(equalTo(1L)));
		assertThat(acaoConsolidada.getValorTotal(), is((equalTo(expectedValorTotal))));
		
		assertThat(acaoConsolidada.getQuantidade(), is(equalTo(expectedQuantidade)));
		assertThat(acaoConsolidada.getPosicaoAtual(), is(notNullValue()));
		assertThat(acaoConsolidada.getPrecoMedio(), is(equalTo(expectedPrecoMedio)));
	}

}
