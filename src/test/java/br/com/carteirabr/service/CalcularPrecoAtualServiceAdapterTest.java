package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.util.SymbolUtil;

@ExtendWith(MockitoExtension.class)
public class CalcularPrecoAtualServiceAdapterTest {

	@InjectMocks
	private CalcularPrecoAtualServiceAdapter calcularPrecoAtualServiceAdapter;

	@Test
	public void givenCotacao_itReturnPrecoAtualFromCotacao() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setPrecoAtual(new BigDecimal("10.33"));
		
		var cotacao = new Cotacao();
		cotacao.setPreco(new BigDecimal("11.65"));

		/* Comprar papel */
		
		BigDecimal precoAtual = calcularPrecoAtualServiceAdapter.calcularPrecoAtual(cotacao, acaoConsolidada);
		
		assertThat(precoAtual, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(11.65)))));
	}
	
	@Test
	public void givenCotacao_withNullValue_itReturnPrecoAtualFromAcaoConsolidada() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setPrecoAtual(new BigDecimal("10.33"));
		
		Cotacao cotacao = null;

		/* Comprar papel */
		
		BigDecimal precoAtual = calcularPrecoAtualServiceAdapter.calcularPrecoAtual(cotacao, acaoConsolidada);
		
		assertThat(precoAtual, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(10.33)))));
	}
}
