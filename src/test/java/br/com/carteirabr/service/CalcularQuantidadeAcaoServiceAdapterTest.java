package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.util.SymbolUtil;

@ExtendWith(MockitoExtension.class)
public class CalcularQuantidadeAcaoServiceAdapterTest {

	@InjectMocks
	private CalcularQuantidadeAcaoServiceAdapter calcularQuantidadeAcaoServiceAdapter;

	@Test
	public void givenSymbol_buy_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularQuantidade(stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(1)))));
	}
	
	@Test
	public void givenSymbol_Sell_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularQuantidade(stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(1)))));
		
		/* Vender papel */
		acaoConsolidada.setQuantidade(new BigDecimal(1));
		stockOrder2.orderType = "V";
		stockOrder2.value = new BigDecimal("70.66");
		stockOrder2.totalValue = new BigDecimal("70.66");
		
		quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularQuantidade(stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(BigDecimal.ZERO))));
	}
	
	@Test
	public void givenSymbol_BuyWithZero_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularQuantidade(stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(1)))));
		
		/* Vender papel */
		acaoConsolidada.setQuantidade(new BigDecimal(1));
		stockOrder2.orderType = "V";
		stockOrder2.value = new BigDecimal("70.66");
		stockOrder2.totalValue = new BigDecimal("70.66");
		
		quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularQuantidade(stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(BigDecimal.ZERO))));
	}
}
