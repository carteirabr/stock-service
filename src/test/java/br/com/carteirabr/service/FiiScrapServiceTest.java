package br.com.carteirabr.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import br.com.carteirabr.adapter.FiiScrapHttpAdapter;
import br.com.carteirabr.domain.model.FiiHistorico;

class FiiScrapServiceTest {

	@Test
	void givenFII_findHistory() {
		FiiScrapHttpAdapter fii = new FiiScrapHttpAdapter();
		List<FiiHistorico> result = fii.getHistoricoFii("HGLG11");
			assertEquals(10, result.size());
			assertNotNull(result.get(0).getCotacaoBase());
			assertNotNull(result.get(0).getDataBase());
			assertNotNull(result.get(0).getDataPagamento());
			assertNotNull(result.get(0).getDy());
			assertNotNull(result.get(0).getRendimento());
			assertNotNull(result.get(0).getSymbol());
			assertNotNull(result.get(0).getTipoFii(), "Tipo FII");
			assertEquals("Tijolo: Galpões", result.get(0).getTipoFii());
			assertEquals("FII CSHG LOG", result.get(0).getNome());
	}

}
