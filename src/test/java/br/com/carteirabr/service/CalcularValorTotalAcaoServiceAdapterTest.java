package br.com.carteirabr.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.util.SymbolUtil;

@ExtendWith(MockitoExtension.class)
public class CalcularValorTotalAcaoServiceAdapterTest {
	
	@InjectMocks
	private CalcularValorTotalAcaoServiceAdapter calcularQuantidadeAcaoServiceAdapter;

	@Test
	public void givenSymbol_buy_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		BigDecimal quantidade = new BigDecimal(1);

		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularValorTotal(quantidade, stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(69.20)))));
	}
	
	@Test
	public void givenSymbol_buyAndSell_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("1");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		BigDecimal quantidade = new BigDecimal(1);

		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularValorTotal(quantidade, stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(69.20)))));
		
		/* Vender papel */
		acaoConsolidada.setQuantidade(new BigDecimal(1));
		stockOrder2.orderType = "V";
		stockOrder2.value = new BigDecimal("70.66");
		stockOrder2.totalValue = new BigDecimal("70.66");
		
		quantidade = SymbolUtil.converterDecimal(BigDecimal.ZERO);
		
		quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularValorTotal(quantidade, stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(70.66)))));
	}
	
	@Test
	public void givenSymbol_buyWithZeroValue_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("0");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		BigDecimal quantidade = new BigDecimal(1);

		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularValorTotal(quantidade, stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(69.20)))));
	}
	
	@Test
	public void givenSymbol_buyWithNegativeValue_shouldReturnValidValue() {
		String symbol = "HGCL11";
		
		AcaoConsolidada acaoConsolidada = new AcaoConsolidada();
		acaoConsolidada.setSymbol(symbol);
		
		Ordem stockOrder2 = new Ordem();
		stockOrder2.symbol = symbol;
		stockOrder2.orderType = "C";
		stockOrder2.data = LocalDate.now();
		stockOrder2.amount = new BigDecimal("0");
		stockOrder2.factor = new BigDecimal("1");
		stockOrder2.value = new BigDecimal("69.20");
		stockOrder2.totalValue = new BigDecimal("69.20");
		
		BigDecimal quantidade = new BigDecimal(1);

		/* Comprar papel */
		BigDecimal quantidadeCalculada = calcularQuantidadeAcaoServiceAdapter.calcularValorTotal(quantidade, stockOrder2, acaoConsolidada);
		
		assertThat(quantidadeCalculada, is(equalTo(SymbolUtil.converterDecimal(new BigDecimal(69.20)))));
	}

}
