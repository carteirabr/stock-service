package br.com.carteirabr.port;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;
import br.com.carteirabr.util.CarteiraQuarkusTest;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;


@QuarkusTest
public class AuthenticationUiPortTest extends CarteiraQuarkusTest {

    @Test
    public void testCadastroUsuario() {
    	Usuario usuario = new Usuario();
    	usuario.setNome("Alexandre");
    	usuario.setSobreNome("Aleixo Wagner");
    	usuario.setEmail("alexandreaw@gmail.com");
    	usuario.setUsername("30167389883");
    	usuario.setPassword("123");
    	
        UsuarioInfo result = given()
          .contentType(ContentType.JSON)
          .body(usuario)
          .when().post("/auth/add-user")
          .then()
          	 .assertThat()
             .statusCode(HttpStatus.SC_OK)
             .extract()
             .as(UsuarioInfo.class);

        assertThat(result, instanceOf(UsuarioInfo.class));
        assertThat(result.getNome(), is(equalTo(usuario.getNome())));
        assertThat(result.getSobreNome(), is(equalTo(usuario.getSobreNome())));
        assertThat(result.getEmail(), is(equalTo(usuario.getEmail())));
        assertThat(result.getUsername(), is(equalTo(usuario.getUsername())));
    }
}