package br.com.carteirabr.port;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import br.com.carteirabr.util.CarteiraQuarkusTest;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;


@QuarkusTest
public class CeiUiPortTest extends CarteiraQuarkusTest {

    @Test
    public void givenCeiData_shouldImportData() throws IOException {
        String contaBancaria = FileUtils.readFileToString(new File("src/test/resources/ceiRicardo.json"), "UTF-8");
		
        given()
          .contentType(ContentType.JSON)
          .body(contaBancaria)
          .when().post("/cei/integration")
          .then()
          	 .assertThat()
             .statusCode(HttpStatus.SC_CREATED);
    }

}