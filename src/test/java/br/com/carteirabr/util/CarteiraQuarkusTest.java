package br.com.carteirabr.util;

import static br.com.carteirabr.util.MongoDbContainer.MONGODB_HOST;
import static br.com.carteirabr.util.MongoDbContainer.MONGODB_PORT;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;

import br.com.carteirabr.configuration.ServiceContext;

@Testcontainers
public class CarteiraQuarkusTest {
	
	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Container
    static GenericContainer MONGO_DB_CONTAINER = new MongoDbContainer()
            .withCreateContainerCmdModifier(cmd -> cmd.withHostName(MONGODB_HOST)
                    .withPortBindings(new PortBinding(Ports.Binding.bindPort(27027), new ExposedPort(MONGODB_PORT))));

	@ConfigProperty(name = "security.default-user", defaultValue = "null") 
	String defaultUser;
	
	@BeforeEach
	public void setUp() {
		ServiceContext.get().overrideCurrentUsername(defaultUser);
	}
}
