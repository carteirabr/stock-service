package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@MongoEntity
@Getter
@Setter
@RegisterForReflection
public class AcaoConsolidada {
	
	public AcaoConsolidada() {
	}
	
	private ObjectId id;
	private String cpf;
	private BigDecimal valorTotal;
	private BigDecimal posicaoAtual;
	private BigDecimal precoAtual;
	private BigDecimal precoMedio;
	private BigDecimal quantidade;
	private BigDecimal lucroPrejuizo;
	private TipoAcao tipo;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime ultimaAtualizacao;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate data;
	private String symbol;
	
	public boolean isOperacaoCompra() {
		return false;
	}
	
	public String getMesAno() {
		return DateTimeFormatter.ofPattern("MM/yyyy").format(data);
	}

	public boolean hasQuantidadePositiva() {
		return quantidade.compareTo(BigDecimal.ZERO) > 0;
	}
}
