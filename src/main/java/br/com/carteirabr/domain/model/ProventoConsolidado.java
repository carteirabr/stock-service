package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@MongoEntity
@Setter
@Getter
@RegisterForReflection
public class ProventoConsolidado {
	
	private ObjectId id;
	private String cpf;
	private BigDecimal total;
	@JsonFormat(pattern = "MM/yyyy")
	private LocalDate data;
}
