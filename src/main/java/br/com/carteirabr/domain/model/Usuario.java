package br.com.carteirabr.domain.model;

import java.beans.Transient;

import org.bson.types.ObjectId;
import org.mindrot.jbcrypt.BCrypt;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.ProjectionFor;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@MongoEntity
@Getter
@Setter
@RegisterForReflection
public class Usuario {
	private ObjectId id;
	private String username;
	private String password;
	private String role;
	private String nome;
	private String sobreNome;
	private String email;
	
	public Usuario() {
	}
    
    @Transient
    public boolean validatePassword(String password) {
    	return BCrypt.checkpw(password, this.password);
	}

    @ProjectionFor(Usuario.class)
    @Getter
    @Setter
    public static class UsuarioInfo {
    	private String username;
    	private String nome;
    	private String sobreNome;
    	private String email;
    }
}