package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.ProjectionFor;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RegisterForReflection
@MongoEntity
public class Provento {
	private ObjectId id;
	private String symbol;
	private String tipoFii;
	private BigDecimal quantidade;
	private BigDecimal total;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate data;
	private String cpf;
	
	public String getMesAno() {
		return DateTimeFormatter.ofPattern("MM/yyyy").format(data);
	}
	
	public double getTotalAsDouble() {
		return total.doubleValue();
	}
	
	@ProjectionFor(Provento.class)
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class ProventoSymbol {
		private String symbol;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ProventoSymbol other = (ProventoSymbol) obj;
			if (symbol == null) {
				if (other.symbol != null)
					return false;
			} else if (!symbol.equals(other.symbol))
				return false;
			return true;
		}
		
		
	}
}
