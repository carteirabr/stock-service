package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.carteirabr.util.SymbolUtil;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.ProjectionFor;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@MongoEntity
@Getter
@Setter
@RegisterForReflection
public class Ordem {
	
	private ObjectId id;
	@JsonFormat(pattern = "dd/MM/yyyy")
	public LocalDate data, expirationDate;
	public String cpf, orderType, orderLocation, symbol, symbolDescription;
	public BigDecimal amount, value, totalValue, factor;

	public String getSymbolNoFraction() {
		return SymbolUtil.removeFacionado(symbol);
	}

	public boolean isOperacaoCompra() {
		return orderType.equals("C");
	}
	
	@ProjectionFor(Ordem.class)
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class OrdensDate {
		private LocalDate data;
	}
	
	@ProjectionFor(Ordem.class)
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class OrdensSymbol {
		private String symbol;
		
		public OrdensSymbol() {
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			OrdensSymbol other = (OrdensSymbol) obj;
			if (symbol == null) {
				if (other.symbol != null)
					return false;
			} else if (!symbol.equals(other.symbol))
				return false;
			return true;
		}
	}
}
