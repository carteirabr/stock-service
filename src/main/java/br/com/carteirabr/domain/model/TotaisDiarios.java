package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RegisterForReflection
@MongoEntity
public class TotaisDiarios {
	
	public TotaisDiarios() {
	}
	
	private ObjectId id;
	private String cpf;
	private BigDecimal valorAplicado;
	private BigDecimal saldoBruto;
	private BigDecimal resultado;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate data;
	private LocalDateTime ultimaAtualizacao;

}
