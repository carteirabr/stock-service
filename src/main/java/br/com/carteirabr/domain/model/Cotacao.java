package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@RegisterForReflection
@JsonRootName(value = "Global Quote")
@JsonIgnoreProperties(ignoreUnknown = true)
@MongoEntity
public class Cotacao {

	public Cotacao() {
	}
	
	private ObjectId id;
	private String symbol;
	
	private BigDecimal preco;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime data;
	
	private BigDecimal precoAbertura;
	private BigDecimal precoMaiorAlta;
	private BigDecimal precoMenorBaixa;
	private BigDecimal precoFechamentoAnterior;
	private String volume;
	private String variacaoMercado;
	private String variacaoMercadoPercentual;
	private String nomeCurto;
	private String nomeLongo;
	private String currencySymbol;
	private String currency;
	private TipoAcao tipo;

}

