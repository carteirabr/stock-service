package br.com.carteirabr.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RegisterForReflection
@MongoEntity
public class FiiHistorico {
	private ObjectId id;
	private String symbol;
	private String tipoFii;
	private String nome;

	@JsonProperty("DataBase")
	@JsonFormat(pattern = "dd/MM/yy")
	private LocalDate dataBase;
	
	@JsonProperty("DataPagamento")
	@JsonFormat(pattern = "dd/MM/yy")
	private LocalDate dataPagamento;
	
	@JsonProperty("CotacaoBase")
	private BigDecimal cotacaoBase;
	
	@JsonProperty("DY")
	private String dy;
	
	@JsonProperty("Rendimento")
	private BigDecimal rendimento;
	
}
