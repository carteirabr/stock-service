package br.com.carteirabr.domain.port;

import br.com.carteirabr.domain.model.Usuario;

public interface AddUserServicePort {

    Usuario addUser(Usuario newUser);

}
