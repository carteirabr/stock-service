package br.com.carteirabr.domain.port;

import java.math.BigDecimal;

import br.com.carteirabr.domain.model.AcaoConsolidada;

public interface CalcularLucroPrejuizoServicePort {

	BigDecimal calcularLucroPrejuizo(BigDecimal quantidade, BigDecimal valorTotal, BigDecimal posicaoAtual,
			AcaoConsolidada acaoConsolidada);

}
