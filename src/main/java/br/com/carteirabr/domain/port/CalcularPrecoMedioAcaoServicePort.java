package br.com.carteirabr.domain.port;

import java.math.BigDecimal;

import br.com.carteirabr.domain.model.AcaoConsolidada;

public interface CalcularPrecoMedioAcaoServicePort {

	BigDecimal calcularPrecoMedio(BigDecimal valorTotal, BigDecimal quantidade, AcaoConsolidada acaoConsolidada);

}
