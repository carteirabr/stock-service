package br.com.carteirabr.domain.port;

import java.math.BigDecimal;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;

public interface CalcularPrecoAtualAcaoServicePort {

	BigDecimal calcularPrecoAtual(Cotacao cotacao, AcaoConsolidada acaoConsolidada);

}
