package br.com.carteirabr.domain.port;

import java.math.BigDecimal;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;

public interface CalcularQuantidadeAcaoServicePort {

	BigDecimal calcularQuantidade(Ordem symbol, AcaoConsolidada acaoConsolidada);

}
