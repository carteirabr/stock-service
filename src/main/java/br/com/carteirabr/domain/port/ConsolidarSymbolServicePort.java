package br.com.carteirabr.domain.port;

import java.time.LocalDate;

import br.com.carteirabr.domain.model.AcaoConsolidada;

public interface ConsolidarSymbolServicePort {

	AcaoConsolidada reConsolidateSymbol(String symbol, LocalDate mesAtual);

	AcaoConsolidada reConsolidateSymbol(String symbol);

}
