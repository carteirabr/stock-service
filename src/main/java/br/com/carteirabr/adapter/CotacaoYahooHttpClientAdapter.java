package br.com.carteirabr.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.io.IOUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.io.URLInputStreamFacade;
import org.jboss.logging.Logger;
import org.jsfr.json.Collector;
import org.jsfr.json.JsonSurfer;
import org.jsfr.json.JsonSurferJackson;
import org.jsfr.json.ValueBox;

import br.com.carteirabr.port.CotacaoClientPort;

@ApplicationScoped
public class CotacaoYahooHttpClientAdapter implements CotacaoClientPort {
	
	private static final Logger LOG = Logger.getLogger(CotacaoYahooHttpClientAdapter.class);

	@Override
	public CotacaoHttp getCotacao(String symbol) {
		LOG.info(String.format("Buscando cotacao [%s] usando Yahoo", symbol));
		
		try {
			String pageContent = extractPageContent(symbol);
			String jsonData = extractJsonData(pageContent);
			
			return parseCotacaoYahoo(jsonData);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
			
			return null;
		}
	}

	private CotacaoHttp parseCotacaoYahoo(String jsonData) {
		JsonSurfer surfer = JsonSurferJackson.INSTANCE;
		Collector collector = surfer.collector(jsonData);
		
		ValueBox<BigDecimal> preco = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketPrice.fmt", BigDecimal.class);
        ValueBox<BigDecimal> precoAbertura = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketOpen.fmt", BigDecimal.class);
        ValueBox<BigDecimal> precoMaiorAlta = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketDayHigh.fmt", BigDecimal.class);
        ValueBox<BigDecimal> precoMenorBaixa = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketDayLow.fmt", BigDecimal.class);
        ValueBox<BigDecimal> precoFechamentoAnterior = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketPreviousClose.fmt", BigDecimal.class);
        ValueBox<String> volume = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketVolume.fmt", String.class);
        ValueBox<String> variacaoMercado = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketChange.fmt", String.class);
        ValueBox<String> variacaoMercadoPercentual = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.regularMarketChangePercent.fmt", String.class);
        ValueBox<String> nomeCurto = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.shortName", String.class);
        ValueBox<String> nomeLongo = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.longName", String.class);
        ValueBox<String> currencySymbol = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.currencySymbol", String.class);
        ValueBox<String> currency = collector.collectOne("$.context.dispatcher.stores.QuoteSummaryStore.price.currency", String.class);
        
        collector.exec();

        CotacaoHttp yahooCotacao = new CotacaoHttp();
        yahooCotacao.preco = preco.get();
        yahooCotacao.precoAbertura = precoAbertura.get();
        yahooCotacao.precoMaiorAlta = precoMaiorAlta.get();
        yahooCotacao.precoMenorBaixa = precoMenorBaixa.get();
        yahooCotacao.precoFechamentoAnterior = precoFechamentoAnterior.get();
        yahooCotacao.volume = volume.get();
        yahooCotacao.variacaoMercado = variacaoMercado.get();
        yahooCotacao.variacaoMercadoPercentual = variacaoMercadoPercentual.get();
        yahooCotacao.nomeCurto = nomeCurto.get();
        yahooCotacao.nomeLongo = nomeLongo.get();
        yahooCotacao.currencySymbol = currencySymbol.get();
        yahooCotacao.currency = currency.get();
        
        return yahooCotacao;
	}

	private String extractPageContent(String symbol) {
		InputStream input = null;
		String pageContent = null;

		try {
			URLInputStreamFacade source = new URLInputStreamFacade(new URL(String.format("https://finance.yahoo.com/quote/%s.sa/", symbol)));
			input = source.getInputStream();
			pageContent = IOUtils.toString(input, StandardCharsets.UTF_8.name());
			
			input.close();
			input = null;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtil.close(input);
		}
		
		return pageContent;
	}
	
	private String extractJsonData(String pageContent) {
		pageContent = pageContent.split("root.App.main = ")[1].split("\n}\\(this\\)\\);", 0)[0];
		return pageContent.substring(0, pageContent.length() - 1);
	}
}
