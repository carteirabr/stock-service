package br.com.carteirabr.adapter;

import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.ProventoConsolidado;
import br.com.carteirabr.port.ProventoConsolidadoRepositoryPort;

@ApplicationScoped
public class ProventoConsolidadoRepositoryAdapter implements ProventoConsolidadoRepositoryPort {

	@Override
	public Stream<ProventoConsolidado> streamAllFromUser() {
		return stream("{ cpf: ?1 }", ServiceContext.get().getCurrentUsername());
	}
}
