package br.com.carteirabr.adapter;

import java.time.LocalDate;
import java.util.stream.Stream;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.port.ConsolidarSymbolServicePort;
import br.com.carteirabr.port.AcaoUIPort;
import br.com.carteirabr.service.AcoesService;

@Path("/acao")
@RolesAllowed("ADMIN")
@Produces("application/json")
@Consumes("application/json")
public class AcaoResourceAdapter implements AcaoUIPort {
	
	@Inject
	AcoesService acoesService;
	
	@Inject
	AcaoConsolidadaRepositoryAdapter acaoConsolidadaRepository;
	
	ConsolidarSymbolServicePort consolidarSymbolService;
	
	@GET
	@Path("/{symbol}")
	@Override
	public AcaoConsolidada getDadosSymbol(@PathParam String symbol) {
		return acoesService.getDadosSymbol(symbol);
	}
	
	@GET
	@Path("/force/{symbol}")
	@Override
	public AcaoConsolidada reProcessarSymbol(@PathParam String symbol) {
		return consolidarSymbolService.reConsolidateSymbol(symbol);
	}
	
	@GET
	@Path("/")
	@Override
	public Stream<AcaoConsolidada> getAll() {
		return acaoConsolidadaRepository.findByMonthYear(LocalDate.now())
				.filter(AcaoConsolidada::hasQuantidadePositiva);
	}
}
