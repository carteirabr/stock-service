package br.com.carteirabr.adapter;

import java.time.LocalDate;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.TotaisDiarios;
import br.com.carteirabr.port.TotaisDiariosRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class TotaisDiariosRepositoryAdapter implements TotaisDiariosRepositoryPort {

	@Override
	public Optional<TotaisDiarios> findByDate(String cpf, LocalDate data) {
		return find("{ cpf: ?1, data: ISODate(?2) }", cpf, SymbolUtil.formatDateIsoString(data))
				.firstResultOptional();
	}
}
