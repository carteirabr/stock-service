package br.com.carteirabr.adapter;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;
import br.com.carteirabr.port.UserRepositoryPort;

@ApplicationScoped
public class UsuarioRepositoryAdapter implements UserRepositoryPort {
	
	@Override
	public UsuarioInfo findByUsername(String currentUsername) {
		return find("{ username: ?1 }", currentUsername)
				.project(UsuarioInfo.class)
				.firstResult();
	}

}
