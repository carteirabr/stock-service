package br.com.carteirabr.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.domain.model.Provento.ProventoSymbol;
import br.com.carteirabr.port.ProventoRepositoryPort;

@ApplicationScoped
public class ProventoRepositoryAdapter implements ProventoRepositoryPort {

	@Override
	public Stream<Provento> findBySymbol(String symbol) {
		return stream("{ cpf: ?1, symbol: ?2 }" , ServiceContext.get().getCurrentUsername(), symbol);
	}

	@Override
	public Stream<Provento> streamAllByUser() {
		return stream("{ cpf: ?1 }" , ServiceContext.get().getCurrentUsername());
	}
	
	@Override
	public Stream<Provento> findAllNoMes(LocalDate data) {
		return stream("{ cpf: ?1, mesAno: ?2 }" , ServiceContext.get().getCurrentUsername(), DateTimeFormatter.ofPattern("MM/yyyy").format(data));
	}
	
	@Override
	public Stream<ProventoSymbol> streamAllDistinctByUser() {
		return findAll()
				.project(ProventoSymbol.class)
				.stream()
				.distinct();
	}
	
}
