package br.com.carteirabr.adapter;

import java.text.ParseException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.carteirabr.port.CeiWebPort;
import br.com.carteirabr.service.CeiService;
import br.com.carteirabr.service.CeiService.CeiResponse;

@Path("/cei")
@Produces("application/json")
@Consumes("application/json")
public class CeiResourceAdapter implements CeiWebPort {

	@Inject
	CeiService ceiService;
	
	@POST
	@Path("/")
	@Override
    public Response create(CeiSyncRequestData data) {
		ceiService.syncCeiData(data.getUsername(), data.getPassword());
		
        return Response.ok().status(201).build();
    }
	
	@POST
	@Path("/integration")
	@Override
    public Response integrate(CeiResponse reponse) throws ParseException {
		ceiService.processAcoes(reponse);
		
        return Response.ok().status(Status.CREATED).build();
    }
}
