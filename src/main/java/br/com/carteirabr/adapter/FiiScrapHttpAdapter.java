package br.com.carteirabr.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.io.IOUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.io.URLInputStreamFacade;
import org.jboss.logging.Logger;

import br.com.carteirabr.domain.model.FiiHistorico;
import br.com.carteirabr.port.FiiScrapServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class FiiScrapHttpAdapter implements FiiScrapServicePort {
	
	private static final Logger LOG = Logger.getLogger(FiiScrapHttpAdapter.class);

	public CompletableFuture<List<FiiHistorico>> getHistoricoFiiFuture(String symbol) {
		final CompletableFuture<List<FiiHistorico>> c1 = new CompletableFuture<>();
		new Thread(() ->  {
			LOG.infov("Buscando historico fii [{0}]", symbol);
			
			var historicos = new ArrayList<FiiHistorico>();
			
			try {
				String pageContent = extractPageContent(symbol);
				
				var infos = extractFiiInfo(pageContent);
				String table = extractTable(pageContent);
//				String tableHeader = extractHeader(table);
				String tableBody = extractBody(table);
				
				String[] lines = tableBody.replaceAll("\\n", "").split("<tr>");
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
				DecimalFormat number = new DecimalFormat("##,###.##");
				
				for (String line : lines) {
					if(!line.startsWith("<td>")) 
						continue;
					
					FiiHistorico fiiResponse = new FiiHistorico();
					fiiResponse.setSymbol(symbol);

					line = line.replace("</tr>", "");
					String[] columns = line.split("<td>");
					for (int i = 0; i < columns.length; i++) {
						String column = columns[i];
						column = column.replace("</td>", "");
						
						if(i == 1) {
							fiiResponse.setDataBase(LocalDate.parse(column, formatter));
						} else if(i == 2) {
							fiiResponse.setDataPagamento(LocalDate.parse(column, formatter));
						} else if(i == 3) {
							BigDecimal value = SymbolUtil.converterDecimal(new BigDecimal(number.parse(column.replace("R$ ", "")).doubleValue()));
							fiiResponse.setCotacaoBase(value);
						} else if(i == 4) {
							fiiResponse.setDy(column);
						} else if(i == 5) {
							BigDecimal value = SymbolUtil.converterDecimal(new BigDecimal(number.parse(column.replace("R$ ", "")).doubleValue()));
							fiiResponse.setRendimento(value);
						}
					}
					
					fiiResponse.setNome(infos.get(0));
					fiiResponse.setTipoFii(infos.get(1));

					historicos.add(fiiResponse);
				}
			} catch (Exception e) {
				c1.completeExceptionally(e);
			}
			
			c1.complete(historicos);
		}).start();
		
		return c1;
	}
	
	@Override
	public List<FiiHistorico> getHistoricoFii(String symbol) {
			LOG.infov("Buscando historico fii [{0}]", symbol);
			
			var historicos = new ArrayList<FiiHistorico>();
			
			try {
				String pageContent = extractPageContent(symbol);
				
				var infos = extractFiiInfo(pageContent);
				String table = extractTable(pageContent);
//				String tableHeader = extractHeader(table);
				String tableBody = extractBody(table);
				
				String[] lines = tableBody.replaceAll("\\n", "").split("<tr>");
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
				DecimalFormat number = new DecimalFormat("##,###.##");
				
				for (String line : lines) {
					if(!line.startsWith("<td>")) 
						continue;
					
					FiiHistorico fiiResponse = new FiiHistorico();
					fiiResponse.setSymbol(symbol);

					line = line.replace("</tr>", "");
					String[] columns = line.split("<td>");
					for (int i = 0; i < columns.length; i++) {
						String column = columns[i];
						column = column.replace("</td>", "");
						
						if(i == 1) {
							fiiResponse.setDataBase(LocalDate.parse(column.trim(), formatter));
						} else if(i == 2) {
							fiiResponse.setDataPagamento(LocalDate.parse(column.trim(), formatter));
						} else if(i == 3) {
							BigDecimal value = SymbolUtil.converterDecimal(new BigDecimal(number.parse(column.replace("R$ ", "")).doubleValue()));
							fiiResponse.setCotacaoBase(value);
						} else if(i == 4) {
							fiiResponse.setDy(column);
						} else if(i == 5) {
							BigDecimal value = SymbolUtil.converterDecimal(new BigDecimal(number.parse(column.replace("R$ ", "")).doubleValue()));
							fiiResponse.setRendimento(value);
						}
					}
					
					fiiResponse.setNome(infos.get(0));
					fiiResponse.setTipoFii(infos.get(1));

					historicos.add(fiiResponse);
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error(e);
			}
		
		return historicos;
	}

	private ArrayList<String> extractFiiInfo(String pageContent) {
		var infos = new ArrayList<String>();
		
		int basicIndex = pageContent.indexOf("informations--basic");
		String basicInfo = pageContent.substring(basicIndex, basicIndex + 300);
		String[] infoValueParse = basicInfo.split("value\">");
		
		for (int i = 1; i < infoValueParse.length; i++) {
			String values = infoValueParse[i];
			String info = values.substring(0, values.indexOf("</span>"));
			
			infos.add(info);
		}
		return infos;
	}
	
	private String extractBody(String table) {
		String headerBeginKey = "<tbody>";
		int headerBeginIndex = table.indexOf(headerBeginKey);
		int headerEndIndex = table.indexOf("</tbody>");
		
		String tableHeader = table.substring(headerBeginIndex + headerBeginKey.length(), headerEndIndex);
		return tableHeader;
	}

	@SuppressWarnings("unused")
	private String extractHeader(String table) {
		String headerBeginKey = "<thead>";
		int headerBeginIndex = table.indexOf(headerBeginKey);
		int headerEndIndex = table.indexOf("</thead>");
		
		String tableHeader = table.substring(headerBeginIndex + headerBeginKey.length(), headerEndIndex);
		return tableHeader;
	}

	private String extractTable(String pageContent) {
		String tableId = "last-revenues--table\">";
		
		int tableBeginIndex = pageContent.indexOf(tableId);
		int tableEndIndex = pageContent.indexOf("</table>", tableBeginIndex);
		
		return pageContent.substring(tableBeginIndex + tableId.length(), tableEndIndex);
	}

	private String extractPageContent(String symbol) {
		InputStream input = null;
		String pageContent = null;

		try {
			URLInputStreamFacade source = new URLInputStreamFacade(new URL(String.format("https://fiis.com.br/%s/", symbol)));
			input = source.getInputStream();
			pageContent = IOUtils.toString(input, StandardCharsets.UTF_8.name());
			
			input.close();
			input = null;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtil.close(input);
		}
		
		return pageContent;
	}
}
