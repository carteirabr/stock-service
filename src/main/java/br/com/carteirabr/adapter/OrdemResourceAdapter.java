package br.com.carteirabr.adapter;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.port.OrdemUiPort;

@Path("/cotacao")
@Produces("application/json")
@Consumes("application/json")
public class OrdemResourceAdapter implements OrdemUiPort {
	
	OrdemRepositoryPort ordemRepository;
    
    @GET
	@Path("/")
    @Override
    public List<Ordem> getAllOrders() {
        return ordemRepository.listAll();
    }
}