package br.com.carteirabr.adapter;

import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.FiiHistorico;
import br.com.carteirabr.port.FiiHistoricoRepositoryPort;

@ApplicationScoped
public class FiiHistoricoRepositoryAdapter implements FiiHistoricoRepositoryPort {

	@Override
	public long countBySymbol(String symbol) {
		return count("{ symbol: ?1 }", symbol);
	}

	@Override
	public Stream<FiiHistorico> streamBySymbol(String symbol) {
		return stream("{ symbol: ?1 }", symbol);
	}
}
