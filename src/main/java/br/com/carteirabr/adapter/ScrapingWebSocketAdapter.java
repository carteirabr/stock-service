package br.com.carteirabr.adapter;

import java.net.URISyntaxException;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.carteirabr.port.FiiUIPort.FiiHistoryResponse;
import br.com.carteirabr.port.ScrapingWebSocketPort;
import br.com.carteirabr.service.CeiService;
import br.com.carteirabr.service.CeiService.CeiResponse;
import br.com.carteirabr.service.FiiService;
import br.com.carteirabr.util.SymbolUtil;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

@ApplicationScoped
public class ScrapingWebSocketAdapter implements ScrapingWebSocketPort {
	
	private static final Logger LOG = Logger.getLogger(ScrapingWebSocketAdapter.class);
	
	@Inject
	ObjectMapper mapper;
	
	@Inject
	FiiService fiiService;
	
	@Inject
	CeiService ceiService;
	
	@Override
	public void requestCeiData(String username, String password) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("user", username);
			obj.put("pass", password);
			
			socket.emit("importData", obj);
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	public void requestFiiData(String symbol) {
		LOG.info(String.format("Requesting fii-data [%s]", symbol));
		try {
			JSONObject obj = new JSONObject();
			obj.put("symbol", SymbolUtil.removeFacionado(symbol));

			socket.emit("fii-import", obj);
		} catch (JSONException e) {
			LOG.error(String.format("Error requesting fii-data [%s]", symbol), e);
		}
	}

	protected void registerEvents(Socket socket) {
		ceiDataHandler(socket);
		fiiDataHandler(socket);
	}

	private void fiiDataHandler(Socket socket) {
		socket.on("fii-data", new Emitter.Listener() {
            @Override
            public void call(Object... objects) {
            	try {
            		JSONObject obj = (JSONObject) objects[0];
            	
            		String jsonString = obj.toString();
            		FiiHistoryResponse reponse = mapper.readValue(jsonString, FiiHistoryResponse.class);
            		
            		LOG.info(String.format("Received fii-data [%s]", reponse.getSymbol()));

            		fiiService.fiiScrapReponse(reponse);
				} catch (JsonProcessingException e) {
					LOG.error("Error parsing fii-data", e);
				}
            }
        });
	}

	private void ceiDataHandler(Socket socket) {
		socket.on("dataImported", new Emitter.Listener() {
            @Override
            public void call(Object... objects) {
            	try {
	            	JSONObject obj = (JSONObject) objects[0];
	                
	            	String jsonString = obj.toString();
	        		CeiResponse reponse = mapper.readValue(jsonString, CeiResponse.class);
	        		
	        		LOG.info(String.format("Received cei-data de [%s]", reponse.getUsername()));
	
	        		ceiService.processAcoes(reponse);
	            } catch (ParseException | JsonProcessingException e) {
					LOG.error("Error parsing cei-data", e);
				}
            }
        });
	}
	
	private static Socket socket;
	
	@ConfigProperty(name = "scraping.url")
	String scrapingUrl;

	@PostConstruct
	public void config() throws URISyntaxException, InterruptedException {
		IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.transports = new String[]{WebSocket.NAME}; 
        
		socket = IO.socket(scrapingUrl, opts);
		
		registerEvents(socket);
		
		socket.connect();
	}

}
