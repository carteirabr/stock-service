package br.com.carteirabr.adapter;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.port.CotacaoRepositoryPort;

@ApplicationScoped
public class CotacaoRepositoryAdapter implements CotacaoRepositoryPort {

	@Override
	public Optional<Cotacao> findBySymbol(String symbol) {
		return find("symbol", symbol).firstResultOptional();
	}
	
}
