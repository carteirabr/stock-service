package br.com.carteirabr.adapter;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.carteirabr.configuration.Token;
import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;
import br.com.carteirabr.port.AuthenticationUiPort;
import br.com.carteirabr.service.AuthenticationService;

@Path("/auth")
@Produces("application/json")
@Consumes("application/json")
public class AuthenticationResourceAdapter implements AuthenticationUiPort {
	
	@Inject
	AuthenticationService authenticationService;
	
    @POST
    @PermitAll
	@Path("/authenticate")
    @Override
    public Response authenticate(Usuario user) {
    	Token token = authenticationService.auth(user);
    	if(token == null)
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	
    	return Response.ok(token).build();
    }
    
    @POST
    @PermitAll
	@Path("/add-user")
    @Override
    public Response addUser(Usuario user) {
    	UsuarioInfo userInfo = authenticationService.addUser(user);
    	return Response.ok(userInfo).build();
    }
    
    @Path("/user-info")
    @GET
    @Override
    public UsuarioInfo getUserLoggedInInfo() {
    	return authenticationService.getUserInfo();
	}
}
