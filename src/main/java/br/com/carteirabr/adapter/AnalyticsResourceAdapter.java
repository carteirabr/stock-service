package br.com.carteirabr.adapter;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.carteirabr.port.AnalyticsUiPort;
import br.com.carteirabr.service.AnalyticsServiceAdapter;
import br.com.carteirabr.service.AnalyticsServiceAdapter.Analytics;

@Path("/analytics")
@Produces("application/json")
@Consumes("application/json")
public class AnalyticsResourceAdapter implements AnalyticsUiPort {
	
	@Inject
	AnalyticsServiceAdapter analyticsService;
	
	@GET
	@Path("/")
	@Override
	public Analytics getTotalBruto() {
		return analyticsService.getTotalBruto();
	}
	
}
