package br.com.carteirabr.adapter;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.port.CotacaoUiPort;
import br.com.carteirabr.service.CotacaoService;

@Path("/cotacao")
@Produces("application/json")
@Consumes("application/json")
public class CotacaoResourceAdapter implements CotacaoUiPort {
	
	@Inject
	CotacaoService cotacaoService;
	
	@GET
	@Path("/force/{symbol}")
	@Override
	public Cotacao forcarAtualizacao(@PathParam String symbol) {
		return cotacaoService.getCotacaoAtualizada(symbol);
	}
	
	@GET
	@PermitAll
	@Path("/{symbol}")
	@Override
	public Cotacao getCotacao(@PathParam String symbol) {
		return cotacaoService.getCotacao(symbol);
	}
	
	@GET
	@Path("/forceall")
	@Override
	public void getAllCotacao(@PathParam String symbol) throws InterruptedException {
		cotacaoService.requestAtualizarCotacoes();
	}
}
