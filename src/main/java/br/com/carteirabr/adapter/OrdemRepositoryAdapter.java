package br.com.carteirabr.adapter;

import java.time.LocalDate;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.model.Ordem.OrdensDate;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class OrdemRepositoryAdapter implements OrdemRepositoryPort {
	
	@Override
	public LocalDate findMinDateBySymbol(String symbol) {
		return find("{ symbol: ?1, cpf: ?2 }", symbol, ServiceContext.get().getCurrentUsername())
				.project(OrdensDate.class)
				.stream()
				.map(OrdensDate::getData)
				.min(LocalDate::compareTo)
				.get();
	}

	@Override
	public Stream<Ordem> findSymbolDataMenorQue(String symbol, LocalDate mesAtual) {
		return stream("{ symbol: ?1, cpf: ?2, data: { '$lte': ISODate(?3) } }", symbol, ServiceContext.get().getCurrentUsername(), SymbolUtil.formatDateIsoString(mesAtual));
	}

}
