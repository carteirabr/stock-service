package br.com.carteirabr.adapter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.domain.model.ProventoConsolidado;
import br.com.carteirabr.port.FiiUIPort;
import br.com.carteirabr.port.ProventoConsolidadoRepositoryPort;
import br.com.carteirabr.port.ProventoRepositoryPort;
import br.com.carteirabr.service.FiiService;

@Path("/fii")
@RolesAllowed("ADMIN")
@Produces("application/json")
@Consumes("application/json")
public class FiiResourceAdapter implements FiiUIPort {
	
	@Inject
	FiiService fiiService;
	
	@Inject
	ProventoConsolidadoRepositoryPort proventoConsolidadoRepository;
	
	@Inject
	ProventoRepositoryPort proventoRepository;
	
	@GET
	@Path("/history-request/{symbol}")
	@Override
	public void getFiiHistory(@PathParam String symbol) {
		fiiService.requestFiiScrap(symbol);
	}
	
	@GET
	@Path("/proventos-resumo")
	@Override
	public Stream<ProventoConsolidado> getProventosMensal() {
		return proventoConsolidadoRepository.streamAll();
	}
	
	@GET
	@Path("/proventos-historico")
	@Override
	public Stream<Provento> getHistorico() {
		return proventoRepository.streamAllByUser()
				.map(Provento.class::cast)
				.sorted(Comparator.comparing(Provento::getData).reversed());
	}
	
	@GET
	@Path("/proventos-grafico")
	@Override
	public DadosGraficoResponse getProventosGrafico() {
		DadosGraficoResponse grafico = new DadosGraficoResponse();
		
		proventoConsolidadoRepository.streamAllFromUser()
				.sorted(Comparator.comparing(ProventoConsolidado::getData))
				.forEach(provento -> grafico.add(provento.getData(), provento.getTotal()));
		
		return grafico;
	}
	
	@GET
	@Path("/proventos-distribuicao")
	@Override
	public List<GraficoProventosDistribuicaoOut> getProventosGraficoDistribuicao() {
		Supplier<Stream<Provento>> streamSupplier = () -> proventoRepository.findAllNoMes(LocalDate.now().minusMonths(1));
		
		var grafico = new ArrayList<GraficoProventosDistribuicaoOut>();

		var total = streamSupplier.get()
			.map(Provento::getQuantidade)
			.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		streamSupplier.get()
				.map(Provento.class::cast)
				.collect(Collectors.groupingBy(Provento::getTipoFii, 
						Collectors.summingDouble(mapper -> (
								mapper.getQuantidade().doubleValue() / total.doubleValue()) *100)))
								.entrySet()
								.stream()
								.forEach(value -> grafico.add(new GraficoProventosDistribuicaoOut(value.getKey(), value.getValue())));
		
		return grafico;
	}

	@POST
	@Path("/integration")
	@Override
	public Response getcons(FiiHistoryResponse response) {
		fiiService.fiiScrapReponse(response);
		
		return Response.ok().status(Status.CREATED).build();
	}
}
