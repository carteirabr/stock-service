package br.com.carteirabr.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;

@ApplicationScoped
public class AcaoConsolidadaRepositoryAdapter implements AcaoConsolidadaRepositoryPort {
	
	@Override
	public Optional<AcaoConsolidada> findBySymbolMonthYearFromUser(String symbol, LocalDate date) {
		var pattern = DateTimeFormatter.ofPattern("MM/yyyy");
		var mesAno = pattern.format(date); 
		
		return stream("{ cpf: ?1, symbol: ?2 } ", ServiceContext.get().getCurrentUsername(), symbol)
				.filter(acao -> pattern.format(((AcaoConsolidada) acao).getData()).equals(mesAno))
				.map(AcaoConsolidada.class::cast)
				.findFirst();
	}
	
	@Override
	public Stream<AcaoConsolidada> findBySymbolMonthYearAllUsers(String symbol, LocalDate date) {
		var pattern = DateTimeFormatter.ofPattern("MM/yyyy");
		var mesAno = pattern.format(date); 
		
		return stream("{ symbol: ?1 } ", symbol)
				.map(AcaoConsolidada.class::cast)
				.filter(acao -> pattern.format(acao.getData()).equals(mesAno));
	}
	
	@Override
	public Stream<AcaoConsolidada> findByMonthYear(String cpf, LocalDate date) {
		var pattern = DateTimeFormatter.ofPattern("MM/yyyy");
		var mesAno = pattern.format(date); 
		
		return stream("{ cpf: ?1 }", cpf)
				.map(AcaoConsolidada.class::cast)
				.filter(acao -> pattern.format(acao.getData()).equals(mesAno));
	}

	@Override
	public Stream<AcaoConsolidada> findByMonthYear(LocalDate now) {
		return findByMonthYear(ServiceContext.get().getCurrentUsername(), now);
	}
	
}
