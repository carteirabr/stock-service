package br.com.carteirabr.configuration;

import java.util.concurrent.atomic.AtomicReference;

public class ServiceContext {

	private ThreadLocal<String> currentUsername = new ThreadLocal<String>();

	private static AtomicReference<ServiceContext> instance = new AtomicReference<>();

	public static void install(ServiceContext context) {
		ServiceContext.instance.set(context);
	}

	public static ServiceContext get() {
		return ServiceContext.instance.get();
	}

	/**
	 * Use this for testing only.
	 */
	public void overrideCurrentUsername(String testUsername) {
		currentUsername.set(testUsername);
	}
	
	public String getCurrentUsername() {
		if(currentUsername.get() == null) {
			throw new RuntimeException("Method getCurrentUsername must be called from request context.");
		}
		return currentUsername.get();
	}


	void setCurrent(String username) {
		currentUsername.set(username);
	}
}