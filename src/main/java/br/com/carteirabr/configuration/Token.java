package br.com.carteirabr.configuration;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@RegisterForReflection
@Getter
@Setter
public class Token {
	public Token() {
	}
	
	public Token(String token) {
		this.token = token;
	}

	private String token;
}
