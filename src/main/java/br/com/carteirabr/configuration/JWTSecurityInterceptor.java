package br.com.carteirabr.configuration;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.jboss.resteasy.core.ResourceMethodInvoker;

import br.com.carteirabr.service.JWTService;
import io.quarkus.runtime.configuration.ConfigurationException;

@Provider
@Priority(Priorities.AUTHORIZATION)
public class JWTSecurityInterceptor implements ContainerRequestFilter {
	
	private static final Logger LOG = Logger.getLogger(JWTSecurityInterceptor.class);

    @Inject
    JWTService jwtService;
    
    @ConfigProperty(name = "security.disabled", defaultValue = "false") 
	boolean securityDisabled;
    
    /**
     * Used only for testing
     */
    @ConfigProperty(name = "security.default-user", defaultValue = "null") 
	String defaultUser;

    @Override
    public void filter(ContainerRequestContext context) {
        final ResourceMethodInvoker resourceMethodInvoker = (ResourceMethodInvoker) context.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        final Method method = resourceMethodInvoker.getMethod();
        
        if(securityDisabled) {
        	if(defaultUser.equals("null")) {
        		throw new ConfigurationException("When security is disabled, [security.default-user] properties must not be null");
        	}
        	
        	ServiceContext.get().setCurrent(defaultUser);
        	
        	LOG.infov("Security is disabled, default user [{0}]", defaultUser);
        	return;
        }

        if (!method.isAnnotationPresent(PermitAll.class)) {
            if (method.isAnnotationPresent(DenyAll.class)) {
                context.abortWith(Response.status(Response.Status.FORBIDDEN).build());
                return;
            }

            final String authorization = context.getHeaderString("Authorization");
            if (authorization == null || authorization.isEmpty() || !authorization.startsWith("Bearer ")) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final String token = authorization.substring("Bearer ".length());
            if (authorization == null || authorization.isEmpty()) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            final String username = jwtService.getUserNameFromToken(token);
            if (username == null) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }
            
			/* Set current request user */
            ServiceContext.get().setCurrent(username);

            final Set<Role> roles = jwtService.getRoles(token);
            if (roles == null || roles.isEmpty()) {
                context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                return;
            }

            if (method.isAnnotationPresent(RolesAllowed.class)) {
                final RolesAllowed rolesAllowedAnnotation = method.getAnnotation(RolesAllowed.class);
                final String[] rolesAllowed = rolesAllowedAnnotation.value();

                if (!Arrays.stream(rolesAllowed).filter(roleAllowed -> roles.contains(Role.valueOf(roleAllowed))).findFirst().isPresent()) {
                    context.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                    return;
                }
            }
        }
    }
}