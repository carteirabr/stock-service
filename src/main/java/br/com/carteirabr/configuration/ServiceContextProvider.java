package br.com.carteirabr.configuration;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import org.jboss.logging.Logger;

import io.quarkus.runtime.StartupEvent;


@ApplicationScoped
public class ServiceContextProvider {
	
	private static final Logger LOG = Logger.getLogger(ServiceContextProvider.class);

	public ServiceContextProvider() {
	}
	
	void onStart(@Observes StartupEvent ev) {               
		LOG.info("Installing Service Context");
		ServiceContext.install(new ServiceContext());
    }
}
