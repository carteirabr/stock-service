package br.com.carteirabr.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class BackendProcessingService {
	
	@Inject
	FiiService fiiService;
	
	@Inject
	CotacaoService cotacaoService;
	
	public static final ExecutorService executor = Executors.newFixedThreadPool(2);
	
	@Scheduled(every="60m", delayed = "1m")
	public void processHistorycoFii() {
		var fii = fiiService.requestFiiHistoryUpdate();
		fii.forEach(job -> executor.submit(job));
	}
	
	@Scheduled(every="5m", delayed = "10s")
	public void processCotacoes() throws InterruptedException {
		var cotacoes = cotacaoService.requestAtualizarCotacoes();
		cotacoes.forEach(job -> executor.submit(job));
	}
}
