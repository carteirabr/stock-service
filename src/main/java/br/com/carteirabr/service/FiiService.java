package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.domain.model.FiiHistorico;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.domain.model.ProventoConsolidado;
import br.com.carteirabr.domain.model.TipoAcao;
import br.com.carteirabr.port.FiiHistoricoRepositoryPort;
import br.com.carteirabr.port.FiiScrapServicePort;
import br.com.carteirabr.port.FiiUIPort.FiiHistoryResponse;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.port.ProventoConsolidadoRepositoryPort;
import br.com.carteirabr.port.ProventoRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class FiiService {
	
	private static final Logger LOG = Logger.getLogger(FiiService.class);
	
	@Inject
	FiiScrapServicePort fiiScrapService;
	
	@Inject
	CotacaoService cotacao;
	
	@Inject
	ProventoRepositoryPort proventoRepository;

	@Inject
	FiiHistoricoRepositoryPort fiiHistoricoRepository;

	@Inject
	OrdemRepositoryPort ordemRepository;

	@Inject
	ProventoConsolidadoRepositoryPort proventoConsolidadoRepository;
	
	/**
	 * Consolida os Proventos em meses
	 * 
	 * @param historico lista de proventos para consolidação
	 * @return retorna o provento consolidado por meses
	 */
	public List<ProventoConsolidado> consolidarProventos(Stream<Provento> historico) {
		Map<String, Double> totais = historico
				.collect(Collectors.groupingBy(Provento::getMesAno, Collectors.summingDouble(Provento::getTotalAsDouble)));
		
		var proventos = new ArrayList<ProventoConsolidado>();
		for (String data : totais.keySet()) {
			
			Double total = totais.get(data);
			if(total == 0) continue;
				
			var proventoConsolidado = new ProventoConsolidado();
			proventoConsolidado.setTotal(SymbolUtil.converterDecimal(new BigDecimal(total)));
			proventoConsolidado.setData(LocalDate.parse("01/" + data, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			proventoConsolidado.setCpf(ServiceContext.get().getCurrentUsername());
			proventos.add(proventoConsolidado);
		}
		
		return proventos;
	}
	
	public List<ProventoConsolidado> consolidarTotaisMensaisProvento() {
		Stream<Provento> proventos = proventoRepository.streamAllByUser();
		var consolidacao = consolidarProventos(proventos);
		
		for (ProventoConsolidado proventoConsolidado : consolidacao) {
			proventoConsolidadoRepository.delete("{ cpf: ?1, data: ISODate(?2) }", proventoConsolidado.getCpf(), SymbolUtil.formatDateIsoString(proventoConsolidado.getData()));
			proventoConsolidadoRepository.persist(proventoConsolidado);
		}
		
		return consolidacao;
	}
	
	public void importarFii(String symbol) {
		Cotacao cotacaoInfo = cotacao.getCotacao(symbol);
		if(!cotacaoInfo.getTipo().equals(TipoAcao.FII)) 
			return;
		
		if(fiiHistoricoRepository.countBySymbol(symbol) > 0)  {
			LOG.info(String.format("Processamento historico fii ignorado [%s]", symbol));
			return;
		}
		
		requestFiiScrap(symbol);
	}
	
	public void requestFiiScrap(String symbol) {
		var historico = fiiScrapService.getHistoricoFii(symbol);
		historico.forEach(fiiHistorico -> saveFiiHistory(fiiHistorico));
	}

	public void fiiScrapReponse(FiiHistoryResponse reponse) {
		for (FiiHistorico entity : reponse.getFiiHistory()) {
			saveFiiHistory(entity);
		}
	}
	
	
	public Stream<Runnable> requestFiiHistoryUpdate() {
		return proventoRepository.streamAllDistinctByUser()
			.map(symbol -> new Runnable() {
				@Override
				public void run() {
					requestFiiScrap(symbol.getSymbol());
				}
			}); 
	}
	
	public FiiHistorico saveFiiHistory(FiiHistorico entity) {
		if(fiiHistoricoRepository.count("{ symbol: ?1, dataBase:  ISODate(?2) }", entity.getSymbol(), SymbolUtil.formatDateIsoString(entity.getDataBase())) > 0) {
			LOG.info(String.format("Ignorando historico ja importado data [%s] symbol [%s]", entity.getDataBase(), entity.getSymbol()));
			return entity;
		}
		
		LOG.info(String.format("Historico importado data [%s] symbol [%s]", entity.getDataBase(), entity.getSymbol()));
		fiiHistoricoRepository.persist(entity);
		
		return entity;
	}
	
	public void gerarProvento(FiiHistorico fii) {
		String symbol = fii.getSymbol();
		BigDecimal amount = ordemRepository.findSymbolDataMenorQue(symbol, fii.getDataPagamento())
			.map(Ordem::getAmount)
			.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		if(amount.intValue() == 0)
			return;
		
		if(proventoRepository.count("{ cpf: ?1, symbol: ?2, data: ISODate(?3) }", ServiceContext.get().getCurrentUsername(), symbol, SymbolUtil.formatDateIsoString(fii.getDataPagamento())) > 0) {
			LOG.infov("Ignorando provento ja consolidado data [{0}] symbol [{1}]", fii.getDataPagamento(), symbol);
			return;
		}
		
		Provento provento = new Provento();
		provento.setSymbol(symbol);
		provento.setQuantidade(amount);
		provento.setTipoFii(fii.getTipoFii());
		provento.setTotal(amount.multiply(fii.getRendimento()));
		provento.setData(fii.getDataPagamento());
		provento.setCpf(ServiceContext.get().getCurrentUsername());
		proventoRepository.persist(provento);
		
		LOG.infov("Consolidado provento [{0}] symbol [{1}]", fii.getDataPagamento(), symbol);
	}
	
	public void gerarProvento(String symbol) {
		fiiHistoricoRepository.streamBySymbol(symbol)
			.forEach(historico -> gerarProvento(historico));
	}

}
