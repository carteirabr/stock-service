package br.com.carteirabr.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;

import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.port.AddUserServicePort;
import br.com.carteirabr.port.UserRepositoryPort;

@ApplicationScoped
public class AddUserServiceAdapter implements AddUserServicePort {

	@Inject
	UserRepositoryPort userRepository;

	@Override
	public Usuario addUser(Usuario user) {
		if(userRepository.count("username", user.getUsername()) > 0) 
			throw new WebApplicationException("Usuario com cpf " + user.getUsername() + " já cadastrado.", Response.Status.BAD_REQUEST);
		
		 user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
		 user.setRole("USER");
		 
		 userRepository.persist(user);
	        
	     return user;
	}
	
	
}
