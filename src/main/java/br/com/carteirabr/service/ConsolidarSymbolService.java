package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.port.CalcularLucroPrejuizoServicePort;
import br.com.carteirabr.domain.port.CalcularPrecoAtualAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularPrecoMedioAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularQuantidadeAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularValorTotalAcaoServicePort;
import br.com.carteirabr.domain.port.ConsolidarSymbolServicePort;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class ConsolidarSymbolService implements ConsolidarSymbolServicePort {
	
	private static final Logger LOG = Logger.getLogger(ConsolidarSymbolService.class);
	
	private CotacaoService cotacaoService; 
	private Event<AcaoConsolidada> event;
	private AcaoConsolidadaRepositoryPort acaoConsolidadaRepository;
	private OrdemRepositoryPort ordemRepository;
	private CalcularQuantidadeAcaoServicePort calcularQuantidadeAcaoService;
	private CalcularValorTotalAcaoServicePort calcularValorTotalAcaoService;
	private CalcularPrecoAtualAcaoServicePort calcularPrecoAtualAcaoService;
	private CalcularPrecoMedioAcaoServicePort calcularPrecoMedioAcaoService;
	private CalcularLucroPrejuizoServicePort calcularLucroPrejuizoService;

	@Inject
	public ConsolidarSymbolService(CotacaoService cotacaoService, Event<AcaoConsolidada> event,
			AcaoConsolidadaRepositoryPort acaoConsolidadaRepository, OrdemRepositoryPort ordemRepository, 
			CalcularQuantidadeAcaoServicePort calcularQuantidadeAcaoService,
			CalcularValorTotalAcaoServicePort calcularValorTotalAcaoService,
			CalcularPrecoAtualAcaoServicePort calcularPrecoAtualAcaoService,
			CalcularPrecoMedioAcaoServicePort calcularPrecoMedioAcaoService,
			CalcularLucroPrejuizoServicePort calcularLucroPrejuizoService) {
		super();
		this.cotacaoService = cotacaoService;
		this.event = event;
		this.acaoConsolidadaRepository = acaoConsolidadaRepository;
		this.ordemRepository = ordemRepository;
		this.calcularQuantidadeAcaoService = calcularQuantidadeAcaoService;
		this.calcularValorTotalAcaoService = calcularValorTotalAcaoService;
		this.calcularPrecoAtualAcaoService = calcularPrecoAtualAcaoService;
		this.calcularPrecoMedioAcaoService = calcularPrecoMedioAcaoService;
		this.calcularLucroPrejuizoService = calcularLucroPrejuizoService;
	}
	
	@Override
	public AcaoConsolidada reConsolidateSymbol(String symbol, LocalDate mesAtual) {
		LOG.info(String.format("Consolidando symbol [%s]", symbol));
		
		if(ordemRepository.count("symbol", symbol) == 0) {
			LOG.warn(String.format("Nenhuma movimentação para symbol [%s]", symbol));
			return null;
		}
		
		/* Checa se Ação ja existe */
		AcaoConsolidada acaoConsolidada = acaoConsolidadaRepository.findBySymbolMonthYearFromUser(symbol, mesAtual)
				.orElseGet(() -> new AcaoConsolidada());
		
		/* Recupera Ordens */
		Supplier<Stream<Ordem>> streamSupplier = () -> ordemRepository.findSymbolDataMenorQue(symbol, mesAtual);
		
		/* Soma Quantidade */
		acaoConsolidada.setQuantidade(BigDecimal.ZERO);
		BigDecimal quantidade = streamSupplier.get()
				.map(value -> calcularQuantidadeAcaoService.calcularQuantidade(value, acaoConsolidada ))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
			
		/* Soma todos os Totais */
		acaoConsolidada.setValorTotal(BigDecimal.ZERO);
		BigDecimal valorTotal = streamSupplier.get()
					.map(value -> calcularValorTotalAcaoService.calcularValorTotal(quantidade, value, acaoConsolidada))
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			
		/* Preço Medio */
		BigDecimal precoMedio = calcularPrecoMedioAcaoService.calcularPrecoMedio(valorTotal, quantidade, acaoConsolidada);
		
		/* Get Cotação Atual */
		Cotacao cotacao = cotacaoService.getCotacao(symbol);

		BigDecimal precoAtual = calcularPrecoAtualAcaoService.calcularPrecoAtual(cotacao, acaoConsolidada);
		
		BigDecimal posicaoAtual = SymbolUtil.converterDecimal(precoAtual.multiply(quantidade));
		
		/* Lucro Prejuizo */
		BigDecimal lucroPrejuizo = calcularLucroPrejuizoService.calcularLucroPrejuizo(quantidade, valorTotal, posicaoAtual, acaoConsolidada);
		
		LocalDateTime dataUltimaAtualizacao = LocalDateTime.now();
		
		acaoConsolidada.setCpf(ServiceContext.get().getCurrentUsername());
		acaoConsolidada.setSymbol(symbol);
		acaoConsolidada.setValorTotal(valorTotal);
		acaoConsolidada.setPosicaoAtual(posicaoAtual);
		acaoConsolidada.setQuantidade(quantidade);
		acaoConsolidada.setPrecoMedio(precoMedio);
		acaoConsolidada.setLucroPrejuizo(lucroPrejuizo);
		acaoConsolidada.setUltimaAtualizacao(dataUltimaAtualizacao);
		acaoConsolidada.setPrecoAtual(precoAtual);
		acaoConsolidada.setData(mesAtual);
		acaoConsolidada.setTipo(cotacao.getTipo());
		
		acaoConsolidadaRepository.persistOrUpdate(acaoConsolidada);
		
		event.fire(acaoConsolidada);

		return acaoConsolidada;
	}
	
	@Override
	public AcaoConsolidada reConsolidateSymbol(String symbol) {
		return reConsolidateSymbol(symbol, LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()));
	}

}
