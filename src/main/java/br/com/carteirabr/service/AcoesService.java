package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.port.CalcularLucroPrejuizoServicePort;
import br.com.carteirabr.domain.port.CalcularPrecoAtualAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularPrecoMedioAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularQuantidadeAcaoServicePort;
import br.com.carteirabr.domain.port.CalcularValorTotalAcaoServicePort;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class AcoesService {
	
	private static final Logger LOG = Logger.getLogger(AcoesService.class);
	
	private CotacaoService cotacaoService; 
	private Event<AcaoConsolidada> event;
	private AcaoConsolidadaRepositoryPort acaoConsolidadaRepository;
	private OrdemRepositoryPort ordemRepository;
	private CalcularQuantidadeAcaoServicePort calcularQuantidadeAcaoService;
	private CalcularValorTotalAcaoServicePort calcularValorTotalAcaoService;
	private CalcularPrecoAtualAcaoServicePort calcularPrecoAtualAcaoService;
	private CalcularPrecoMedioAcaoServicePort calcularPrecoMedioAcaoService;
	private CalcularLucroPrejuizoServicePort calcularLucroPrejuizoService;
	private ConsolidarSymbolService consolidarSymbolService;

	@Inject
	public AcoesService(CotacaoService cotacaoService, Event<AcaoConsolidada> event,
			AcaoConsolidadaRepositoryPort acaoConsolidadaRepository, OrdemRepositoryPort ordemRepository, 
			CalcularQuantidadeAcaoServicePort calcularQuantidadeAcaoService,
			CalcularValorTotalAcaoServicePort calcularValorTotalAcaoService,
			CalcularPrecoAtualAcaoServicePort calcularPrecoAtualAcaoService,
			CalcularPrecoMedioAcaoServicePort calcularPrecoMedioAcaoService,
			CalcularLucroPrejuizoServicePort calcularLucroPrejuizoService,
			ConsolidarSymbolService consolidarSymbolService) {
		super();
		this.cotacaoService = cotacaoService;
		this.event = event;
		this.acaoConsolidadaRepository = acaoConsolidadaRepository;
		this.ordemRepository = ordemRepository;
		this.calcularQuantidadeAcaoService = calcularQuantidadeAcaoService;
		this.calcularValorTotalAcaoService = calcularValorTotalAcaoService;
		this.calcularPrecoAtualAcaoService = calcularPrecoAtualAcaoService;
		this.calcularPrecoMedioAcaoService = calcularPrecoMedioAcaoService;
		this.calcularLucroPrejuizoService = calcularLucroPrejuizoService;
		this.consolidarSymbolService = consolidarSymbolService;
	}
	
	public void mudancaCotacaoHandler(@Observes(during = TransactionPhase.AFTER_SUCCESS) Cotacao cotacao) {
		LOG.info(String.format("Consolidando mudanca de cotacao para symbol [%s]", cotacao.getSymbol()));
		
		var result = acaoConsolidadaRepository.findBySymbolMonthYearAllUsers(cotacao.getSymbol(), LocalDate.now());
		if(result.count() == 0) {
			result = acaoConsolidadaRepository.findBySymbolMonthYearAllUsers(cotacao.getSymbol(), LocalDate.now().minusMonths(1));	
		}
			
		result.forEach(acaoConsolidada -> {
			var acaoConsolidadaChanged = new AcaoConsolidada();
			LocalDate date = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
			acaoConsolidadaChanged.setData(date);
			
			if(acaoConsolidada.getData().equals(date)) {
				acaoConsolidadaChanged = acaoConsolidada;
				acaoConsolidadaChanged.setData(acaoConsolidada.getData());
			}
			
			BigDecimal precoAtual = cotacao.getPreco();
			BigDecimal quantidade = acaoConsolidada.getQuantidade();
			
			BigDecimal posicaoAtual = precoAtual
					.multiply(quantidade);
			
			BigDecimal lucroPrejuizo = posicaoAtual
					.subtract(acaoConsolidada.getValorTotal());
			
			acaoConsolidadaChanged.setCpf(acaoConsolidada.getCpf());
			acaoConsolidadaChanged.setSymbol(acaoConsolidada.getSymbol());
			acaoConsolidadaChanged.setValorTotal(acaoConsolidada.getValorTotal());
			acaoConsolidadaChanged.setPosicaoAtual(posicaoAtual);
			acaoConsolidadaChanged.setQuantidade(quantidade);
			acaoConsolidadaChanged.setPrecoMedio(acaoConsolidada.getPrecoMedio());
			acaoConsolidadaChanged.setLucroPrejuizo(lucroPrejuizo);
			acaoConsolidadaChanged.setUltimaAtualizacao(LocalDateTime.now());
			acaoConsolidadaChanged.setPrecoAtual(precoAtual);
			acaoConsolidadaChanged.setTipo(cotacao.getTipo());
			
			acaoConsolidadaRepository.persistOrUpdate(acaoConsolidadaChanged);

			/* Dispara evento de Mudança Totais */
			event.fire(acaoConsolidadaChanged);
		});
	}

	public void consolidaHistorico(String symbol) {
		LocalDate dataInicial = ordemRepository.findMinDateBySymbol(symbol);
		LocalDate dataAtual = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
		
		while(dataInicial.isBefore(dataAtual) || dataInicial.isEqual(dataAtual)) {
			LOG.info(String.format("Consolidando historico symbol [%s], mes/ano [%d/%d]", symbol, dataInicial.getMonthValue(), dataInicial.getYear()));
			
			consolidarSymbolService.reConsolidateSymbol(symbol, dataAtual);
			dataAtual = dataAtual.minusMonths(1);
		}
	}
	
	public AcaoConsolidada consolidateSymbol(Ordem symbol) {
		LOG.info(String.format("Consolidando symbol [%s]", symbol));
		
		LocalDate mesAtual = symbol.getData();
		
		/* Checa se Ação ja existe */
		AcaoConsolidada acaoConsolidada = acaoConsolidadaRepository.findBySymbolMonthYearFromUser(symbol.getSymbol(), mesAtual)
				.orElseGet(() -> new AcaoConsolidada());
		
		/* Permite so uma atualizacao em menos de 1 hora */
		LocalDateTime dataUltimaAtualizacao = Optional.<LocalDateTime>ofNullable(acaoConsolidada.getUltimaAtualizacao())
				.orElseGet(() -> LocalDateTime.now());
		
		/* Quantidade Ações */
		BigDecimal quantidade = calcularQuantidadeAcaoService.calcularQuantidade(symbol, acaoConsolidada);

		/* Total Value */
		BigDecimal valorTotal = calcularValorTotalAcaoService.calcularValorTotal(quantidade, symbol, acaoConsolidada);
		
		/* Preço Medio */
		BigDecimal precoMedio = calcularPrecoMedioAcaoService.calcularPrecoMedio(valorTotal, quantidade, acaoConsolidada);

		/* Get Cotação Atual */
		Cotacao cotacao = cotacaoService.getCotacao(symbol.getSymbol());
		
		/* PrecoAtual Atual */
		BigDecimal precoAtual = calcularPrecoAtualAcaoService.calcularPrecoAtual(cotacao, acaoConsolidada);
			
		BigDecimal posicaoAtual = SymbolUtil.converterDecimal(precoAtual.multiply(quantidade));
		
		/* Lucro Prejuizo */
		BigDecimal lucroPrejuizo = calcularLucroPrejuizoService.calcularLucroPrejuizo(quantidade, valorTotal, posicaoAtual, acaoConsolidada);
			
		dataUltimaAtualizacao = LocalDateTime.now();

		acaoConsolidada.setCpf(ServiceContext.get().getCurrentUsername());
		acaoConsolidada.setSymbol(symbol.getSymbol());
		acaoConsolidada.setValorTotal(valorTotal);
		acaoConsolidada.setPosicaoAtual(posicaoAtual);
		acaoConsolidada.setQuantidade(quantidade);
		acaoConsolidada.setPrecoMedio(precoMedio);
		acaoConsolidada.setLucroPrejuizo(lucroPrejuizo);
		acaoConsolidada.setUltimaAtualizacao(dataUltimaAtualizacao);
		acaoConsolidada.setPrecoAtual(precoAtual);
		acaoConsolidada.setData(mesAtual);
		acaoConsolidada.setTipo(cotacao.getTipo());
		acaoConsolidadaRepository.persistOrUpdate(acaoConsolidada);
		
		event.fire(acaoConsolidada);
		
		return acaoConsolidada;
	}
	
	public AcaoConsolidada getDadosSymbol(String symbol) {
		return (AcaoConsolidada) acaoConsolidadaRepository.find("symbol", symbol.toUpperCase())
				.firstResultOptional()
				.orElseGet(() -> consolidarSymbolService.reConsolidateSymbol(symbol));
	}
}
