package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.port.CalcularQuantidadeAcaoServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class CalcularQuantidadeAcaoServiceAdapter implements CalcularQuantidadeAcaoServicePort {
	
	@Override
	public BigDecimal calcularQuantidade(Ordem symbol, AcaoConsolidada acaoConsolidada) {
		BigDecimal quantidade = Optional.<BigDecimal>ofNullable(acaoConsolidada.getQuantidade())
				.map(action -> action )
				.orElseGet(() -> BigDecimal.ZERO);
		
		BigDecimal quantidadeOperacaoAtual = symbol
				.getAmount()
				.multiply(symbol.getFactor());
		
		if(symbol.isOperacaoCompra()) {
			return SymbolUtil.converterDecimal(quantidade.add(quantidadeOperacaoAtual));
		}
		
		return SymbolUtil.converterDecimal(quantidade.subtract(quantidadeOperacaoAtual));
	}
}
