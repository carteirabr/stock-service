package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.port.ScrapingWebSocketPort;
import br.com.carteirabr.util.SymbolUtil;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;


@ApplicationScoped
public class CeiService {
	
	private static final Logger LOG = Logger.getLogger(CeiService.class);
	
	@Inject
	ScrapingWebSocketPort scraping;
	
	@Inject
	AcoesService acoes;
	
	@Inject
	FiiService fii;

	@Inject
	AnalyticsServiceAdapter analytics;
	
	@Inject
	OrdemRepositoryPort ordemRepository;
	
	public String syncCeiData(String username, String password) {
		scraping.requestCeiData(username, password);
		
		return "Atualização solicitada";
	}

	public void processAcoes(CeiResponse reponse) throws ParseException {
		ServiceContext.get().overrideCurrentUsername(reponse.getUsername());
		
		var listSymboldForEvent = new HashSet<String>();
		
		List<CeiDataResponse> ceiDataList = reponse.getCeiData();
		for (CeiDataResponse ceiDataResponse : ceiDataList) {
			List<OrdensResponse> orderList = ceiDataResponse.getData();
			for (OrdensResponse ordenResponse : orderList) {
				Ordem ordem = ordenResponse.toOrdens();

				if(ordemRepository.count("{ symbol: ?1, data: { '$eq': ISODate(?2) }, amount: ?3, orderType: ?4 }", ordenResponse.getSymbol(), SymbolUtil.formatDateIsoString(ordem.getData()), ordem.getAmount(), ordem.getOrderType()) > 0) {
					LOG.info(String.format("Ignorando movimentacao symbol [%s], ja importada", ordenResponse.getSymbol()));
					continue;
				}
				
				LOG.info(String.format("Importada nova movimentacao symbol [%s]", ordenResponse.getSymbol()));
				
				ordem.setCpf(reponse.getUsername());
				ordemRepository.persist(ordem);
				
				listSymboldForEvent.add(ordem.getSymbol());
			}
		}

		/* Fire change event */
		for (String symbol : listSymboldForEvent) {
			acoes.consolidaHistorico(symbol);
			fii.importarFii(symbol);
			fii.gerarProvento(symbol);
		}
		
		if(listSymboldForEvent.size() > 0) {
			fii.consolidarTotaisMensaisProvento();
//			analytics.consolidadaTotaisDiariosAcoes(null);
		}
	}
	
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class CeiDataResponse {
		String name;
		List<OrdensResponse> data;
	}
	
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class CeiResponse {
		
		String username;
		List<CeiDataResponse> ceiData;
	}	
	
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class OrdensResponse {
		
		@JsonProperty("Especificação do Ativo")
		private String symbolDescription;
	    
		@JsonProperty("Valor Total(R$)")
	    private String totalValue;
		
		@JsonProperty("Data do Negócio")
		@JsonFormat(pattern = "dd/MM/yyyy")
	    public LocalDate date; 
		
		@JsonProperty("Compra/Venda")
	    public String orderType; 
		
		@JsonProperty("Fator de Cotação")
	    public BigDecimal factor;
		
		@JsonProperty("Preço (R$)")
	    public String value;
		
		@JsonProperty("Código Negociação")
		public String symbol; 
	    
		@JsonProperty("Prazo/Vencimento")
		@JsonFormat(pattern = "dd/MM/yyyy")
	    public LocalDate expirationDate;

		@JsonProperty("Mercado")
		public String orderLocation; 
		
		@JsonProperty("Quantidade")
		public BigDecimal amount;
		
		public Ordem toOrdens() throws ParseException {
			DecimalFormat number = new DecimalFormat("##,###.##");
			BigDecimal totalValueBigDecimal = SymbolUtil.converterDecimal(new BigDecimal(number.parse(totalValue).doubleValue()));
			BigDecimal valueBigDecimal = SymbolUtil.converterDecimal(new BigDecimal(number.parse(value).doubleValue()));
			
			Ordem stockOrder = new Ordem();
			stockOrder.setSymbol(symbol);
			stockOrder.setSymbolDescription(symbolDescription);
			stockOrder.setTotalValue(totalValueBigDecimal);
			stockOrder.setData(date);
			stockOrder.setOrderType(orderType);
			stockOrder.setFactor(factor);
			stockOrder.setValue(valueBigDecimal);
			stockOrder.setExpirationDate(expirationDate);
			stockOrder.setOrderLocation(orderLocation);
			stockOrder.setAmount(amount);
			
			return stockOrder;
		}
	}
}
