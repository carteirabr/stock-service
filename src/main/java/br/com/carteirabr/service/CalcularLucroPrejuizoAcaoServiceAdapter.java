package br.com.carteirabr.service;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.port.CalcularLucroPrejuizoServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class CalcularLucroPrejuizoAcaoServiceAdapter implements CalcularLucroPrejuizoServicePort {

	@Override
	public BigDecimal calcularLucroPrejuizo(BigDecimal quantidade, BigDecimal valorTotal, BigDecimal posicaoAtual, AcaoConsolidada acaoConsolidada) {
		if(quantidade.equals(SymbolUtil.converterDecimal(BigDecimal.ZERO))) {
			return valorTotal.subtract(acaoConsolidada.getValorTotal());
		}
		
//		if(!symbol.isOperacaoCompra()) {
//			return valorTotal.subtract(posicaoAtual);
//		}
		
		return posicaoAtual.subtract(valorTotal);
	}
}
