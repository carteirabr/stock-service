package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.port.CalcularPrecoMedioAcaoServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class CalcularPrecoMedioAcaoServiceAdapter implements CalcularPrecoMedioAcaoServicePort {
	
	@Override
	public BigDecimal calcularPrecoMedio(BigDecimal valorTotal, BigDecimal quantidade, AcaoConsolidada acaoConsolidada) {
		if(quantidade.equals(BigDecimal.ZERO)) return BigDecimal.ZERO;
		
		if(quantidade.equals(SymbolUtil.converterDecimal(BigDecimal.ZERO))) {
			return acaoConsolidada.getPrecoMedio();
		}
		
		return valorTotal.divide(quantidade, 2, RoundingMode.HALF_UP);
	}

}
