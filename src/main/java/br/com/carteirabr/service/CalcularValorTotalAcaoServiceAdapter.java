package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Ordem;
import br.com.carteirabr.domain.port.CalcularValorTotalAcaoServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class CalcularValorTotalAcaoServiceAdapter implements CalcularValorTotalAcaoServicePort {
	
	@Override
	public BigDecimal calcularValorTotal(BigDecimal quantidade, Ordem ordens, AcaoConsolidada acaoConsolidada) {
		BigDecimal valorTotal = Optional.<BigDecimal>ofNullable(acaoConsolidada.getValorTotal())
				.orElseGet(() -> BigDecimal.ZERO);
		
		if(quantidade.equals(SymbolUtil.converterDecimal(BigDecimal.ZERO))) {
			return ordens.getTotalValue();
		}
		
		if(ordens.isOperacaoCompra()) {
			return valorTotal.add(ordens.getTotalValue());
		}
		
		return valorTotal.subtract(ordens.getTotalValue());
	}
}
