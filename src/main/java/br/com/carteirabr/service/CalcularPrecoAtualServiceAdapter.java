package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.logging.Logger;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.domain.port.CalcularPrecoAtualAcaoServicePort;
import br.com.carteirabr.util.SymbolUtil;

@ApplicationScoped
public class CalcularPrecoAtualServiceAdapter implements CalcularPrecoAtualAcaoServicePort {
	
	private static final Logger LOG = Logger.getLogger(CalcularPrecoAtualServiceAdapter.class);

	@Override
	public BigDecimal calcularPrecoAtual(Cotacao cotacao, AcaoConsolidada acaoConsolidada) {
		try {
			return SymbolUtil.converterDecimal(
					cotacao
						.getPreco());
			
		} catch (Exception e) {
			LOG.warn(String.format("Preço atual nao esta disponivel [%s]", acaoConsolidada.getSymbol()));
			
			return SymbolUtil.converterDecimal(
					Optional.ofNullable(acaoConsolidada.getPrecoAtual())
							.orElseGet(() -> BigDecimal.ZERO));
		}
	}
}
