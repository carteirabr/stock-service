package br.com.carteirabr.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.configuration.Token;
import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;
import br.com.carteirabr.port.UserRepositoryPort;
import io.quarkus.security.UnauthorizedException;

@ApplicationScoped
public class AuthenticationService {
	
	private static final Logger LOG = Logger.getLogger(AuthenticationService.class);
	
	@Inject
	JWTService jwtService;

	@Inject
	UserRepositoryPort userRepository;
	
	@Inject
	AddUserServiceAdapter addUserServiceAdapter;
	
	public Token auth(Usuario user) {
		Usuario userEntity = (Usuario) userRepository.find("{ 'username' : ?1 }", user.getUsername())
				.firstResultOptional()
				.orElseThrow(UnauthorizedException::new);
		
		if(!userEntity.validatePassword(user.getPassword())) {
			throw new UnauthorizedException();
		}
		
		LOG.info(String.format("Usuario autenticado [%s]", user.getUsername()));
		
		return jwtService.addToken(user);
	}
	
	public void logout(String token) {
		jwtService.removeToken(token);
	}

	public UsuarioInfo addUser(Usuario user) {
		user = addUserServiceAdapter.addUser(user);
		return userRepository.findByUsername(user.getUsername());
	}
	
	public UsuarioInfo getUserInfo() {
		return userRepository.findByUsername(ServiceContext.get().getCurrentUsername());
	}
}
