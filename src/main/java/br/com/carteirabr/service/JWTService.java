package br.com.carteirabr.service;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Singleton;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import br.com.carteirabr.configuration.Role;
import br.com.carteirabr.configuration.Token;
import br.com.carteirabr.domain.model.Usuario;

@Singleton
public class JWTService {
	
	private final Set<String> tokens = new HashSet<>();
	private final Algorithm algorithm = Algorithm.HMAC256("secret");

	public Set<Role> getRoles(String token) {
		Set<Role> roles = new HashSet<Role>();
		roles.add(Role.ADMIN);
		return roles;
	}

	public String getUserNameFromToken(String token) {
		if(!tokens.contains(token)) {
			return null;
		}
		
		try {
		    JWTVerifier verifier = JWT.require(algorithm)
		        .withIssuer("auth0")
		        .build(); 
		    
		    DecodedJWT jwt = verifier.verify(token);
		    return jwt.getSubject();
		} catch (JWTVerificationException exception){
		}
		
		return null;
	}

	public Token addToken(Usuario user) {
		try {
    	    String token = JWT.create()
    	        .withIssuer("auth0")
    	        .withSubject(user.getUsername())
    	        .sign(algorithm);
    	    ;

    	    tokens.add(token);
    	    return new Token(token);
    	} catch (JWTCreationException exception){
    	}
		
		return null;
	}

	public void removeToken(String token) {
		tokens.remove(token);
	}

}
