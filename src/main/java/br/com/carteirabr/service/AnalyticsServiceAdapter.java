package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.carteirabr.configuration.ServiceContext;
import br.com.carteirabr.domain.model.AcaoConsolidada;
import br.com.carteirabr.domain.model.TotaisDiarios;
import br.com.carteirabr.port.AcaoConsolidadaRepositoryPort;
import br.com.carteirabr.port.TotaisDiariosRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@ApplicationScoped
public class AnalyticsServiceAdapter {
	
	@Inject
	AcaoConsolidadaRepositoryPort acaoConsolidadaRepository;
	
	@Inject
	TotaisDiariosRepositoryPort totaisGeraisRepository;
	
	private static final Logger LOG = Logger.getLogger(AnalyticsServiceAdapter.class);
	
	public Analytics getTotalBruto() {
		Supplier<Stream<TotaisDiarios>> streamSupplier = () -> totaisGeraisRepository.stream("{ cpf: ?1 }", ServiceContext.get().getCurrentUsername());
		
		List<BigDecimal> resultadoList = streamSupplier.get()
				.map(value -> ((TotaisDiarios) value).getResultado())
				.collect(Collectors.toList());
		
		List<BigDecimal> saldoBrutoList = streamSupplier.get()
				.map(value -> ((TotaisDiarios) value).getSaldoBruto())
				.collect(Collectors.toList());
		
		List<BigDecimal> valorAplicadoList = streamSupplier.get()
				.map(value -> ((TotaisDiarios) value).getValorAplicado())
				.collect(Collectors.toList());
		
		List<LocalDate> dataList = streamSupplier.get()
				.map(value -> ((TotaisDiarios) value).getData())
				.collect(Collectors.toList());
		
		Analytics totalEntity = new Analytics();
		totalEntity.setResultado(resultadoList);
		totalEntity.setSaldoBruto(saldoBrutoList);
		totalEntity.setValorAplicado(valorAplicadoList);
		totalEntity.setData(dataList);
		
		return totalEntity;
	}

	private BigDecimal calcularValorInvestido(AcaoConsolidada value) {
		if(value.getQuantidade().equals(SymbolUtil.converterDecimal(BigDecimal.ZERO))) {
			return BigDecimal.ZERO;
		}
		
		return value.getValorTotal();
	}

	public void consolidadaTotaisDiariosAcoes(@Observes AcaoConsolidada acao) {
		LOG.info(String.format("Consolidando totais diarios."));
		
		Supplier<Stream<AcaoConsolidada>> streamSupplier = () -> acaoConsolidadaRepository.findByMonthYear(acao.getCpf(), LocalDate.now());
		
		BigDecimal saldoBruto = streamSupplier.get()
				.map(value -> value.getPosicaoAtual() )
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		BigDecimal valorAplicado = streamSupplier.get()
				.map(value -> calcularValorInvestido(value) )
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		
		BigDecimal resultado = saldoBruto.subtract(valorAplicado);
		
		TotaisDiarios totaisDiariosEntity = totaisGeraisRepository.findByDate(acao.getCpf(), LocalDate.now())
				.orElseGet(() -> {
					TotaisDiarios totaisDiarios = new TotaisDiarios();
					totaisDiarios.setData(LocalDate.now());
					return totaisDiarios;
				});
		
		totaisDiariosEntity.setCpf(acao.getCpf());
		totaisDiariosEntity.setSaldoBruto(saldoBruto);
		totaisDiariosEntity.setResultado(resultado);
		totaisDiariosEntity.setValorAplicado(valorAplicado);
		totaisDiariosEntity.setUltimaAtualizacao(LocalDateTime.now());
		
		totaisGeraisRepository.persistOrUpdate(totaisDiariosEntity);
	}
	
	@RegisterForReflection
	@Getter
	@Setter
	@Generated
	public static class Analytics {
		
		public Analytics() {
		}
		
		private List<BigDecimal> valorAplicado;
		
		private List<BigDecimal> saldoBruto;
		
		private List<BigDecimal> resultado;
		
		@JsonFormat(pattern = "dd/MM/yyyy")
		private List<LocalDate> data;
	}
}
