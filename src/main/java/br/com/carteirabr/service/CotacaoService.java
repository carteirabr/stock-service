package br.com.carteirabr.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.jboss.logging.Logger;

import br.com.carteirabr.domain.model.Cotacao;
import br.com.carteirabr.domain.model.TipoAcao;
import br.com.carteirabr.domain.model.Ordem.OrdensSymbol;
import br.com.carteirabr.port.CotacaoClientPort;
import br.com.carteirabr.port.CotacaoClientPort.CotacaoHttp;
import br.com.carteirabr.port.CotacaoRepositoryPort;
import br.com.carteirabr.port.OrdemRepositoryPort;
import br.com.carteirabr.util.SymbolUtil;


@ApplicationScoped
public class CotacaoService {
	
	private static final Logger LOG = Logger.getLogger(CotacaoService.class);
	
	@Inject
	CotacaoClientPort cotacaoClient;
	
	@Inject
	Event<Cotacao> event;

	@Inject
	CotacaoRepositoryPort cotacaoRepository;
	
	@Inject
	OrdemRepositoryPort ordemRepository;
	
	@Transactional
	public Cotacao getCotacaoAtualizada(String symbol) {
		symbol = symbol.toUpperCase();
		
		Cotacao cotacao = cotacaoRepository.findBySymbol(symbol)
				.orElseGet(() -> new Cotacao());
		
		LOG.info(String.format("Atualizando cotacao do symbol [%s]", symbol));
		BigDecimal preco = cotacao.getPreco();

		CotacaoHttp yahooCotacao = cotacaoClient.getCotacao(SymbolUtil.removeFacionado(symbol));
		if(yahooCotacao != null && yahooCotacao.getPreco() != null) {
			preco = yahooCotacao.getPreco();
			
			cotacao.setPreco(preco);
			cotacao.setPrecoAbertura(yahooCotacao.getPrecoAbertura());
			cotacao.setPrecoMaiorAlta(yahooCotacao.getPrecoMaiorAlta());
			cotacao.setPrecoMenorBaixa(yahooCotacao.getPrecoMenorBaixa());
			cotacao.setPrecoFechamentoAnterior(yahooCotacao.getPrecoFechamentoAnterior());
			cotacao.setVolume(yahooCotacao.getVolume());
			cotacao.setVariacaoMercado(yahooCotacao.getVariacaoMercado());
			cotacao.setVariacaoMercadoPercentual(yahooCotacao.getVariacaoMercadoPercentual());
			cotacao.setNomeCurto(yahooCotacao.getNomeCurto());
			cotacao.setNomeLongo(yahooCotacao.getNomeLongo());
			cotacao.setCurrencySymbol(yahooCotacao.getCurrencySymbol());
			cotacao.setCurrency(yahooCotacao.getCurrency());
			cotacao.setTipo(yahooCotacao.getNomeCurto().toUpperCase().contains("FII") ? TipoAcao.FII : TipoAcao.ACAO);
			
			cotacao.setData(LocalDateTime.now());
			cotacao.setSymbol(symbol);
			
			cotacaoRepository.persistOrUpdate(cotacao);
			
			/* Dispara evento de Mudança Cotacao */
			event.fire(cotacao);
		}
		
		return cotacao;
	}
	
	public Cotacao getCotacao(String symbol) {
		final String symbolUpper = SymbolUtil.removeFacionado(symbol.toUpperCase());
		
		return cotacaoRepository.findBySymbol(symbolUpper)
				.orElseGet(() -> getCotacaoAtualizada(symbolUpper));
	}
	
	public Stream<Runnable> requestAtualizarCotacoes() throws InterruptedException {
		LOG.info("Buscando novas cotações");

		return ordemRepository
			.findAll()
			.project(OrdensSymbol.class)
			.stream()
			.distinct()
			.map(value -> new Runnable() {
					@Override
					public void run() {
						getCotacaoAtualizada(value.getSymbol()); 
					}
				});
	}

}
