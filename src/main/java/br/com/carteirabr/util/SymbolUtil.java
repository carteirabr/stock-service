package br.com.carteirabr.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SymbolUtil {
	public static String removeFacionado(String symbol) {
		if(symbol.toLowerCase().endsWith("f")) {
			symbol = symbol.substring(0, symbol.length() - 1);
		}
		
		return symbol;
	}
	
	public static BigDecimal converterDecimal(BigDecimal bigDecimal) {
		return bigDecimal.setScale(2, RoundingMode.HALF_UP);
	}
	
	public static String formatDateIsoString(LocalDate data) {
		return data.format(DateTimeFormatter.ISO_DATE) + "T00:00:00.000Z";
	}
}
