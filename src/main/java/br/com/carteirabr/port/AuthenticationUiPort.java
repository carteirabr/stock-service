package br.com.carteirabr.port;

import javax.ws.rs.core.Response;

import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;

public interface AuthenticationUiPort {

	Response authenticate(Usuario user);

	Response addUser(Usuario user);

	UsuarioInfo getUserLoggedInInfo();

}
