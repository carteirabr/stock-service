package br.com.carteirabr.port;

import java.util.List;

import br.com.carteirabr.domain.model.Ordem;

public interface OrdemUiPort {

	List<Ordem> getAllOrders();

}
