package br.com.carteirabr.port;

import java.time.LocalDate;
import java.util.stream.Stream;

import br.com.carteirabr.domain.model.Ordem;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface OrdemRepositoryPort extends PanacheMongoRepository<Ordem> {

	LocalDate findMinDateBySymbol(String symbol);

	Stream<Ordem> findSymbolDataMenorQue(String symbol, LocalDate mesAtual);

}
