package br.com.carteirabr.port;

import java.util.stream.Stream;

import br.com.carteirabr.domain.model.AcaoConsolidada;

public interface AcaoUIPort {

	AcaoConsolidada getDadosSymbol(String symbol);

	AcaoConsolidada reProcessarSymbol(String symbol);

	Stream<AcaoConsolidada> getAll();

}
