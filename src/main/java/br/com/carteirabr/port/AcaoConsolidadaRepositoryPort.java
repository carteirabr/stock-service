package br.com.carteirabr.port;

import java.time.LocalDate;
import java.util.Optional;
import java.util.stream.Stream;

import br.com.carteirabr.domain.model.AcaoConsolidada;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface AcaoConsolidadaRepositoryPort extends PanacheMongoRepository<AcaoConsolidada> {

	Optional<AcaoConsolidada> findBySymbolMonthYearFromUser(String symbol, LocalDate date);

	Stream<AcaoConsolidada> findBySymbolMonthYearAllUsers(String symbol, LocalDate date);

	Stream<AcaoConsolidada> findByMonthYear(String cpf, LocalDate date);

	Stream<AcaoConsolidada> findByMonthYear(LocalDate now);

}
