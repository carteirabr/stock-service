package br.com.carteirabr.port;

import java.time.LocalDate;
import java.util.Optional;

import br.com.carteirabr.domain.model.TotaisDiarios;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface TotaisDiariosRepositoryPort extends PanacheMongoRepository<TotaisDiarios> {

	Optional<TotaisDiarios> findByDate(String cpf, LocalDate data);

}
