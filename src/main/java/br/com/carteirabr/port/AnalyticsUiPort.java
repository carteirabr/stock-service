package br.com.carteirabr.port;

import br.com.carteirabr.service.AnalyticsServiceAdapter.Analytics;

public interface AnalyticsUiPort {

	Analytics getTotalBruto();

}
