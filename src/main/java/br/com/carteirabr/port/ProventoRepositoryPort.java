package br.com.carteirabr.port;

import java.time.LocalDate;
import java.util.stream.Stream;

import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.domain.model.Provento.ProventoSymbol;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface ProventoRepositoryPort extends PanacheMongoRepository<Provento> {

	Stream<ProventoSymbol> streamAllDistinctByUser();

	Stream<Provento> findAllNoMes(LocalDate data);

	Stream<Provento> streamAllByUser();

	Stream<Provento> findBySymbol(String symbol);

}
