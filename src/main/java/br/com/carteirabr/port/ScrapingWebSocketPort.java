package br.com.carteirabr.port;

public interface ScrapingWebSocketPort {

	void requestCeiData(String username, String password);

}
