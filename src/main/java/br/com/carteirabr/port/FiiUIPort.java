package br.com.carteirabr.port;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.carteirabr.domain.model.FiiHistorico;
import br.com.carteirabr.domain.model.Provento;
import br.com.carteirabr.domain.model.ProventoConsolidado;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

public interface FiiUIPort {

	Response getcons(FiiHistoryResponse response);

	List<GraficoProventosDistribuicaoOut> getProventosGraficoDistribuicao();

	DadosGraficoResponse getProventosGrafico();

	Stream<Provento> getHistorico();

	Stream<ProventoConsolidado> getProventosMensal();

	void getFiiHistory(String symbol);

	@RegisterForReflection
	@Getter
	@Setter
	@Generated
	public static class DadosGraficoResponse {
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MMM/yyyy", locale = "pt-BR", timezone = "Brazil/East")
		List<LocalDate> datas = new ArrayList<LocalDate>();
		List<BigDecimal> totais = new ArrayList<BigDecimal>();
		
		BigDecimal valorMaximo = BigDecimal.ZERO;
		
		public DadosGraficoResponse add(LocalDate data, BigDecimal total) {
			datas.add(data);
			totais.add(total);
			
			if(total.compareTo(valorMaximo) > 0) {
				valorMaximo = total;
			}
			
			return this;
		}
		
	}
	
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class GraficoProventosDistribuicaoOut {
		
		private String name;
		private Double value;

		public GraficoProventosDistribuicaoOut(String name, Double value) {
			this.name = name;
			this.value = value;
		}

	}
	
	@Getter
	@Setter
	@RegisterForReflection
	@Generated
	public static class FiiHistoryResponse {
		
		public FiiHistoryResponse() {
		}
		
		String symbol;
		
		List<FiiHistorico> fiiHistory;
	}
}
