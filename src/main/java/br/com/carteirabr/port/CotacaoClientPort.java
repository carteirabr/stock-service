package br.com.carteirabr.port;

import java.math.BigDecimal;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;

public interface CotacaoClientPort {

	
	@Getter
	@RegisterForReflection
	public class CotacaoHttp {
		public BigDecimal preco;
		public BigDecimal precoAbertura;
		public BigDecimal precoMaiorAlta;
		public BigDecimal precoMenorBaixa;
		public BigDecimal precoFechamentoAnterior;
		public String volume;
		public String variacaoMercado;
		public String variacaoMercadoPercentual;
		public String nomeCurto;
		public String nomeLongo;
		public String currencySymbol;
		public String currency;
	}

	CotacaoHttp getCotacao(String symbol);
}
