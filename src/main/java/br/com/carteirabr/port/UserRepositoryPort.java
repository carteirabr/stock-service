package br.com.carteirabr.port;

import br.com.carteirabr.domain.model.Usuario;
import br.com.carteirabr.domain.model.Usuario.UsuarioInfo;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface UserRepositoryPort extends PanacheMongoRepository<Usuario> {

	UsuarioInfo findByUsername(String currentUsername);

}
