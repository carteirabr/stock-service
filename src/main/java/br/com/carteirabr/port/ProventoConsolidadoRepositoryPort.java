package br.com.carteirabr.port;

import java.util.stream.Stream;

import br.com.carteirabr.domain.model.ProventoConsolidado;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface ProventoConsolidadoRepositoryPort extends PanacheMongoRepository<ProventoConsolidado> {

	Stream<ProventoConsolidado> streamAllFromUser();

}
