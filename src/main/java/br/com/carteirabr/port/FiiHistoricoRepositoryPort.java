package br.com.carteirabr.port;

import java.util.stream.Stream;

import br.com.carteirabr.domain.model.FiiHistorico;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface FiiHistoricoRepositoryPort extends PanacheMongoRepository<FiiHistorico> {

	long countBySymbol(String symbol);

	Stream<FiiHistorico> streamBySymbol(String symbol);

}
