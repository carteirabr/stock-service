package br.com.carteirabr.port;

import java.util.List;

import br.com.carteirabr.domain.model.FiiHistorico;

public interface FiiScrapServicePort {

	List<FiiHistorico> getHistoricoFii(String symbol);

}
