package br.com.carteirabr.port;

import java.text.ParseException;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonRootName;

import br.com.carteirabr.service.CeiService.CeiResponse;
import lombok.Data;
import lombok.Generated;

public interface CeiWebPort {

	Response create(CeiSyncRequestData data);

	Response integrate(CeiResponse reponse) throws ParseException;

	@Data
	@JsonRootName("ceisync")
	@Generated
	public static class CeiSyncRequestData {
		
		public CeiSyncRequestData() {
		}

		public CeiSyncRequestData(String username, String password) {
			this.username = username;
			this.password = password;
		}
		
		String username;
		String password;
	}
}
