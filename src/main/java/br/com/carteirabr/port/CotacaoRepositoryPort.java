package br.com.carteirabr.port;

import java.util.Optional;

import br.com.carteirabr.domain.model.Cotacao;
import io.quarkus.mongodb.panache.PanacheMongoRepository;

public interface CotacaoRepositoryPort extends PanacheMongoRepository<Cotacao> {

	Optional<Cotacao> findBySymbol(String symbol);

}
