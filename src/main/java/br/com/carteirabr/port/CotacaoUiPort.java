package br.com.carteirabr.port;

import br.com.carteirabr.domain.model.Cotacao;

public interface CotacaoUiPort {

	Cotacao forcarAtualizacao(String symbol);

	Cotacao getCotacao(String symbol);

	void getAllCotacao(String symbol) throws InterruptedException;

}
